package com.besttech.oxy

import com.besttech.oxy.bean.WebInfo
import com.besttech.oxy.button.SoftJButton
import com.besttech.oxy.color.MyColor
import org.cef.CefApp
import org.cef.CefClient
import org.cef.CefSettings
import org.cef.browser.CefBrowser
import org.cef.browser.CefFrame
import org.cef.handler.CefAppHandlerAdapter
import org.cef.handler.CefLoadHandlerAdapter
import org.cef.network.CefCookieManager
import org.cef.network.CefRequest
import java.awt.*
import java.awt.event.WindowAdapter
import java.awt.event.WindowEvent
import java.util.*
import javax.swing.*
import kotlin.collections.HashMap
import kotlin.system.exitProcess

class MainFrame(
        private val map: HashMap<Int, WebInfo>, useOSR: Boolean, isTransparent: Boolean
    ) : JFrame() {
    private val cefApp: CefApp
    private val client: CefClient
    private val browser: CefBrowser
    private val browserUI: Component
    private var datas: BooleanArray
    private var buttonSelected = 0
    private val buttons: Array<SoftJButton?>
    private val cefCookieManager: CefCookieManager
    private var ipCurrent: String? = null
    private fun selectButton(index: Int) {
        buttonSelected = index
        buttons[index]?.background = MyColor.successColor
        ipCurrent = map[index]?.ip
        browser.loadURL(ipCurrent)
    }

    internal inner class LoadHandler : CefLoadHandlerAdapter() {
        private var onceLoadJquery = true
        override fun onLoadStart(cefBrowser: CefBrowser, cefFrame: CefFrame, transitionType: CefRequest.TransitionType) {
            super.onLoadStart(cefBrowser, cefFrame, transitionType)
            if (browser.url == "$ipCurrent/auth") {
                try {
                    Thread.sleep(50)
                } catch (e: InterruptedException) {
                    e.printStackTrace()
                }
                val code = """
                    var xhr = new XMLHttpRequest();
                    xhr.open('POST', '/login', true);
                    xhr.setRequestHeader('Content-Type', 'application/json');
                    xhr.send(JSON.stringify({
                      username: 'admin',
                      'password': '71ce2ad3d895a0e9bbbe7d1dc360472a',
                      'authentication': 'basic'
                    }));"""
                browser.executeJavaScript(code, "", 0)
                try {
                    Thread.sleep(50)
                } catch (e: InterruptedException) {
                    e.printStackTrace()
                }
                browser.loadURL(ipCurrent)
            } else if (browser.url == "$ipCurrent/#/") {
                if (onceLoadJquery) {
                    // Đẩy jQuery vào đây
                    browser.executeJavaScript("", "", 0)
                    onceLoadJquery = false
                }
            }
        }

        override fun onLoadEnd(browser: CefBrowser, cefFrame: CefFrame, i: Int) {
            if (browser.url == "$ipCurrent/#/") {
            }
        }
    }

    init {
        CefApp.addAppHandler(object : CefAppHandlerAdapter(null) {
            override fun stateHasChanged(state: CefApp.CefAppState) {
                // Shutdown the app if the native CEF part is terminated
                if (state == CefApp.CefAppState.TERMINATED) exitProcess(0)
            }
        })
        val settings = CefSettings()
        settings.windowless_rendering_enabled = useOSR
        cefApp = CefApp.getInstance(settings)
        client = cefApp.createClient()
        browser = client.createBrowser(map[0]?.ip, useOSR, isTransparent)
        browserUI = browser.uiComponent
        // TODO
        //** Hacker code here */
        val panel = JPanel()
        panel.preferredSize = Dimension(200, 200)
        panel.background = Color.white
        val boxLayout = BoxLayout(panel, BoxLayout.Y_AXIS)
        panel.layout = boxLayout
        val size = map.size
        datas = BooleanArray(size)
        Arrays.fill(datas, true)
        buttons = arrayOfNulls(size)
        for (i in 0 until size) {
            buttons[i] = SoftJButton(map[i]?.name)
            buttons[i]?.foreground = Color.WHITE
            buttons[i]?.background = MyColor.facebookColor
            buttons[i]?.alignmentX = Component.LEFT_ALIGNMENT //0.0
            panel.add(buttons[i])

            // Space
            panel.add(JLabel(" "))
        }

        // Button mac dinh
        selectButton(buttonSelected)

        // Chay su kien khi click button
        for (i in 0 until size) {
            buttons[i]?.addActionListener {
                // Set cac button khac mau xanh tru button bi disable
                for (j in 0 until size) {
                    if (j != i && datas[j]) {
                        buttons[j]?.background = MyColor.facebookColor
                    }
                }
                // Chọn button
                selectButton(i)
            }
        }
        contentPane.add(panel, BorderLayout.WEST)
        contentPane.add(browserUI, BorderLayout.CENTER)
        defaultCloseOperation = WindowConstants.EXIT_ON_CLOSE
        background = Color.white
        pack()
        device.fullScreenWindow = this
        isVisible = true
        addWindowListener(object : WindowAdapter() {
            override fun windowClosing(e: WindowEvent) {
                CefApp.getInstance().dispose()
                dispose()
            }
        })

        client.addLoadHandler(LoadHandler())
        cefCookieManager = CefCookieManager.getGlobalManager()
    }
}

private val device: GraphicsDevice = GraphicsEnvironment
    .getLocalGraphicsEnvironment().screenDevices[0]
fun main(args: Array<String>) {
    // Khởi tạo một HashMap lưu dữ liệu
    val map: HashMap<Int, WebInfo> = HashMap()
    map[0] = WebInfo("YOUTUBE", "www.youtube.com")
    if (!CefApp.startup(args)) {
        println("Startup initialization failed!")
        return
    }
    MainFrame(map, useOSR = false, isTransparent = false)
}