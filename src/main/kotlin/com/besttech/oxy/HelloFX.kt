package com.besttech.oxy

import javafx.application.Application
import javafx.scene.Scene
import javafx.scene.control.Label
import javafx.scene.control.Tab
import javafx.scene.control.TabPane
import javafx.stage.Stage

class HelloFX : Application() {
    // launch the application
    override fun start(stage: Stage) {
        try {

            // set title for the stage
            stage.title = "creating Tab"

            // create Tab
            val tab1 = Tab("Tab_1")
            val tab2 = Tab("Tab_2")

            // create a label
            val label = Label(" This is a tab ")

            // add label to the tab
            tab1.content = label

            // create a input stream
//            val input = FileInputStream("f:\\gfg.png")
//
//            // create a image
//            val image = Image(input)
//
//            // create a image View
//            val imageview = ImageView(image)
//
//            // add graphic to the tab
//            tab1.graphic = imageview

            // add tab
            // create a tabpane
            val tabpane = TabPane(tab1, tab2)

            // create a scene
            val scene = Scene(tabpane, 600.0, 500.0)

            // set the scene
            stage.scene = scene
            stage.show()
        } catch (e: Exception) {
            System.err.println(e.message)
        }
    }

}

fun main() {

    // launch the application
    Application.launch(HelloFX::class.java)
}

