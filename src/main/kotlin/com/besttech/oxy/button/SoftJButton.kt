package com.besttech.oxy.button

import com.besttech.oxy.color.MyColor
import java.awt.AlphaComposite
import java.awt.Color
import java.awt.Graphics
import java.awt.Graphics2D
import javax.swing.Icon
import javax.swing.JButton
import javax.swing.plaf.metal.MetalButtonUI

class SoftJButton @JvmOverloads constructor(text: String? = null, icon: Icon? = null) : JButton(text, icon) {
    private var rectangularLAF = false
    private var alpha = 1f
    fun getAlpha(): Float {
        return alpha
    }

    fun setAlpha(alpha: Float) {
        this.alpha = alpha
        repaint()
    }

    override fun paintComponent(g: Graphics) {
        val g2: Graphics2D = g as Graphics2D
        g2.composite = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, alpha)
        if (rectangularLAF && isBackgroundSet) {
            val c: Color = background
            g2.color = c
            g.fillRect(0, 0, width, height)
        }
        super.paintComponent(g2)
    }

    override fun updateUI() {
        super.updateUI()
        lafDeterminer.updateUI()
        rectangularLAF = lafDeterminer.isOpaque
    }

    companion object {
        private val lafDeterminer: JButton = JButton()
        private const val serialVersionUID = 1L
    }

    init {
        isOpaque = false
        isFocusPainted = false
        isBorderPainted = false
        isFocusPainted = false
        isContentAreaFilled = false
        setUI(object : MetalButtonUI() {
            override fun getDisabledTextColor(): Color {
                return MyColor.disableColor
            }
        })
    }
}