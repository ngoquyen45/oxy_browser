package com.besttech.oxy.color

import java.awt.Color

interface MyColor {
    companion object {
        val facebookColor = Color(26, 115, 232)
        val disableColor = Color(128, 128, 128)
        val successColor = Color(66, 168, 70)
        val lightWhite = Color(240, 240, 240)
    }
}