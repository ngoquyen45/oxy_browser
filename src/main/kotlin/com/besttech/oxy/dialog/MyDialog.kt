package com.besttech.oxy.dialog

import org.cef.browser.CefBrowser
import com.besttech.oxy.button.SoftJButton
import com.besttech.oxy.color.MyColor
import com.besttech.oxy.filter.IpFilter
import java.awt.Component
import java.awt.Font
import javax.swing.*
import javax.swing.text.PlainDocument

class MyDialog(panel: JPanel, frame: JFrame, browser: CefBrowser) : JDialog() {
    init {
        val font = Font("SansSerif", Font.PLAIN, 18)
        val nameField = JFormattedTextField()
        nameField.setFont(font)
        val ipField = JTextField()
        ipField.setFont(font)

        // Ngăn chăn nhập sai
        (ipField.getDocument() as PlainDocument).setDocumentFilter(IpFilter())
        val inputs: Array<JComponent> = arrayOf(
            JLabel("Tên Thermal "), nameField,
            JLabel("Địa chỉ IP"), ipField
        )
        JOptionPane.showMessageDialog(null, inputs, "Thêm thiết bị", JOptionPane.PLAIN_MESSAGE)
        val name: String = nameField.getText()
        val ip: String = ipField.getText()

        // TODO Check xem du lieu co hop le ko
//        thermalDeviceInfo.setIp(ip);
//        thermalDeviceInfo.setName(name);
        val softJButton = SoftJButton(name)
        softJButton.setSize(200, 200)
        softJButton.foreground = MyColor.facebookColor
        softJButton.alignmentX = Component.LEFT_ALIGNMENT //0.0
        softJButton.toolTipText = ip
        softJButton.addActionListener { l -> browser.loadURL(softJButton.getText()) }

        // Thêm sự kiện cho nút bấm
        panel.add(softJButton)
        panel.revalidate()
        frame.validate()
    }
}