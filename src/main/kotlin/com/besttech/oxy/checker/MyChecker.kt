package com.besttech.oxy.checker

import okhttp3.OkHttpClient
import okhttp3.Request
import com.besttech.oxy.bean.WebInfo
import java.util.concurrent.TimeUnit

class MyChecker(private val map: HashMap<Int, WebInfo>) {
    private var size = 0
    fun checkFull(): BooleanArray {
        val result = BooleanArray(size)
        val threads = arrayOfNulls<Thread>(size)
        for (i in 0 until size) {
            threads[i] = Thread { result[i] = map[i]?.let { checkOnce(it.ip) } == true }
            threads[i]!!.start()
        }
        for (i in 0 until size) {
            while (true) {
                try {
                    threads[i]!!.join()
                    break
                } catch (ignored: InterruptedException) {
                }
            }
        }
        return result
    }

    private fun checkOnce(ip: String): Boolean {
        val client: OkHttpClient = OkHttpClient.Builder()
            .connectTimeout(4, TimeUnit.SECONDS)
            .build()
        val request: Request = Request.Builder()
            .url(ip)
            .build()
        try {
            client.newCall(request).execute().use { return true }
        } catch (e: Exception) {
            return false
        }
    }

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            val map: HashMap<Int, WebInfo> = HashMap()
            map[0] = WebInfo("Google", "https://www.google.com")
            map[1] = WebInfo("Facebook", "https://www.facebook.com")
            map[2] = WebInfo("Facebook", "http://192.168.51.16")
            println(MyChecker(map).checkFull().contentToString())
        }
    }

    init {
        size = map.size
    }
}