// Copyright (c) 2014 The Chromium Embedded Framework Authors. All rights
// reserved. Use of this source code is governed by a BSD-style license that
// can be found in the LICENSE file.
package com.besttech.test.dialog

import org.cef.callback.CefWebPluginInfoVisitor
import org.cef.network.CefWebPluginInfo
import org.cef.network.CefWebPluginManager
import java.awt.BorderLayout
import java.awt.Frame
import java.util.*
import javax.swing.*
import javax.swing.table.AbstractTableModel

class WebPluginManagerDialog(owner: Frame?, title: String?) : JDialog(owner, title, false) {
    private val manager = CefWebPluginManager.getGlobalManager()
    private val tblModel: PluginTableModel = PluginTableModel()

    private inner class PluginTableModel : AbstractTableModel(), CefWebPluginInfoVisitor {
        private val columnNames: Array<String> = arrayOf("Name", "Path", "Version", "Description", "")
        private val rowData = Vector<Array<Any>>()

        // add an entry to the table
        override fun visit(info: CefWebPluginInfo, count: Int, total: Int): Boolean {
            val entry = arrayOf<Any>(
                info.name, info.path, info.version,
                info.description, false
            )
            val row = rowData.size
            rowData.addElement(entry)
            fireTableRowsInserted(row, row)
            return true
        }

        fun removeSelected() {
            var i = 0
            while (i < rowData.size) {
                if (rowData[i][4] as Boolean) {
                    rowData[i][1] as String
                    rowData.removeAt(i)
                    fireTableRowsDeleted(i, i)
                    i--
                }
                ++i
            }
            manager!!.refreshPlugins()
        }

        override fun getRowCount(): Int {
            return rowData.size
        }

        override fun getColumnCount(): Int {
            return columnNames.size
        }

        override fun getColumnName(column: Int): String {
            return columnNames[column]
        }

        override fun getColumnClass(columnIndex: Int): Class<*> {
            return if (rowData.size > 0) rowData[0][columnIndex].javaClass else Any::class.java
        }

        override fun isCellEditable(rowIndex: Int, columnIndex: Int): Boolean {
            return columnIndex == 4
        }

        override fun getValueAt(rowIndex: Int, columnIndex: Int): Any {
            return rowData[rowIndex][columnIndex]
        }

        override fun setValueAt(aValue: Any, rowIndex: Int, columnIndex: Int) {
            rowData[rowIndex][columnIndex] = aValue
            fireTableCellUpdated(rowIndex, columnIndex)
        }

    }

    init {
        layout = BorderLayout()
        setSize(800, 600)
        val pluginTable = JTable(tblModel)
        pluginTable.fillsViewportHeight = true
        val controlPanel = JPanel()
        controlPanel.layout = BoxLayout(controlPanel, BoxLayout.X_AXIS)
        val delButton = JButton("Remove selected plugins")
        delButton.addActionListener { tblModel.removeSelected() }
        controlPanel.add(delButton)
        val doneButton = JButton("Done")
        doneButton.addActionListener { isVisible = false }
        controlPanel.add(doneButton)
        add(JScrollPane(pluginTable))
        add(controlPanel, BorderLayout.SOUTH)
        if (manager == null) throw NullPointerException("Plugin manager is null")
        manager.visitPlugins(tblModel)
    }
}