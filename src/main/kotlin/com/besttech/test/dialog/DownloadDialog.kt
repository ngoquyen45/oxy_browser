// Copyright (c) 2014 The Chromium Embedded Framework Authors. All rights
// reserved. Use of this source code is governed by a BSD-style license that
// can be found in the LICENSE file.
package com.besttech.test.dialog

import org.cef.browser.CefBrowser
import org.cef.callback.CefBeforeDownloadCallback
import org.cef.callback.CefDownloadItem
import org.cef.callback.CefDownloadItemCallback
import org.cef.handler.CefDownloadHandler
import java.awt.BorderLayout
import java.awt.Color
import java.awt.Dimension
import java.awt.Frame
import java.util.*
import javax.swing.*
import kotlin.math.ln
import kotlin.math.pow

class DownloadDialog(owner: Frame) : JDialog(owner, "Downloads", false), CefDownloadHandler {

    private val downloadObjects: MutableMap<Int, DownloadObject> = HashMap()
    private val downloadPanel = JPanel()
    private val dialog: DownloadDialog

    private inner class DownloadObject(downloadItem: CefDownloadItem, suggestedName: String?) :
        JPanel() {
        private var isHidden = true
        private val identifier: Int
        private val fileName = JLabel()
        private val status = JLabel()
        private val dlAbort = JButton()
        private val dlRemoveEntry = JButton("x")
        private var callback: CefDownloadItemCallback? = null
        private val bgColor: Color

        // The method humanReadableByteCount() is based on
        // http://stackoverflow.com/questions/3758606/how-to-convert-byte-size-into-human-readable-format-in-java
        fun humanReadableByteCount(bytes: Long): String {
            val unit = 1024
            if (bytes < unit) return "$bytes B"
            val exp = (ln(bytes.toDouble()) / ln(unit.toDouble())).toInt()
            val pre = "" + "kMISTYPE"[exp - 1]
            return String.format("%.1f %sB", bytes / unit.toDouble().pow(exp.toDouble()), pre)
        }

        fun update(downloadItem: CefDownloadItem, callback: CefDownloadItemCallback?) {
            val percentComplete = downloadItem.percentComplete
            val rcvBytes = humanReadableByteCount(downloadItem.receivedBytes)
            val totalBytes = humanReadableByteCount(downloadItem.totalBytes)
            val speed = humanReadableByteCount(downloadItem.currentSpeed) + "it/s"
            if (downloadItem.receivedBytes >= 5 && isHidden) {
                dialog.isVisible = true
                dialog.toFront()
                owner.toBack()
                isHidden = false
            }
            Runtime.getRuntime().runFinalization()
            this.callback = callback
            status.text = (rcvBytes + " of " + totalBytes + " - " + percentComplete + "%"
                    + " - " + speed)
            dlAbort.isEnabled = downloadItem.isInProgress
            dlRemoveEntry.isEnabled = (!downloadItem.isInProgress || downloadItem.isCanceled
                    || downloadItem.isComplete)
            if (!downloadItem.isInProgress && !downloadItem.isCanceled
                && !downloadItem.isComplete
            ) {
                fileName.text = "FAILED - " + fileName.text
                callback!!.cancel()
            }
        }

        init {
            isOpaque = true
            layout = BorderLayout()
            maximumSize = Dimension(dialog.width - 10, 80)
            identifier = downloadItem.id
            bgColor = if (identifier % 2 == 0) Color.WHITE else Color.YELLOW
            background = bgColor
            fileName.text = suggestedName
            add(fileName, BorderLayout.NORTH)
            status.alignmentX = LEFT_ALIGNMENT
            add(status, BorderLayout.CENTER)
            val controlPane = JPanel()
            controlPane.layout = BoxLayout(controlPane, BoxLayout.X_AXIS)
            controlPane.isOpaque = true
            controlPane.background = bgColor
            dlAbort.text = "Abort"
            dlAbort.isEnabled = false
            dlAbort.addActionListener {
                if (callback != null) {
                    fileName.text = "ABORTED - " + fileName.text
                    callback!!.cancel()
                }
            }
            controlPane.add(dlAbort)
            dlRemoveEntry.isEnabled = false
            dlRemoveEntry.addActionListener {
                val removed = downloadObjects.remove(identifier)
                if (removed != null) {
                    downloadPanel.remove(removed)
                    dialog.repaint()
                }
            }
            controlPane.add(dlRemoveEntry)
            add(controlPane, BorderLayout.SOUTH)
            update(downloadItem, null)
        }
    }

    override fun onBeforeDownload(
        browser: CefBrowser, downloadItem: CefDownloadItem,
        suggestedName: String, callback: CefBeforeDownloadCallback
    ) {
        callback.Continue(suggestedName, true)
        val dlObject = DownloadObject(downloadItem, suggestedName)
        downloadObjects[downloadItem.id] = dlObject
        downloadPanel.add(dlObject)
    }

    override fun onDownloadUpdated(
        browser: CefBrowser, downloadItem: CefDownloadItem, callback: CefDownloadItemCallback
    ) {
        val dlObject = downloadObjects[downloadItem.id] ?: return
        dlObject.update(downloadItem, callback)
    }

    init {
        isVisible = false
        setSize(400, 300)
        dialog = this
        downloadPanel.layout = BoxLayout(downloadPanel, BoxLayout.Y_AXIS)
        add(downloadPanel)
    }
}