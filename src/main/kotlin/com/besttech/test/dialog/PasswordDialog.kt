// Copyright (c) 2014 The Chromium Embedded Framework Authors. All rights
// reserved. Use of this source code is governed by a BSD-style license that
// can be found in the LICENSE file.
package com.besttech.test.dialog

import org.cef.callback.CefAuthCallback
import java.awt.Frame
import java.awt.GridLayout
import java.awt.event.ActionListener
import javax.swing.*

class PasswordDialog(owner: Frame?, private val callback_: CefAuthCallback) :
    JDialog(owner, "Authentication required", true), Runnable {
    private val username = JTextField(20)
    private val password = JPasswordField(20)
    override fun run() {
        isVisible = true
    }

    init {
        setSize(400, 100)
        layout = GridLayout(0, 2)
        add(JLabel("Username:"))
        add(username)
        add(JLabel("Password:"))
        add(password)
        val abortButton = JButton("Abort")
        abortButton.addActionListener {
            callback_.cancel()
            isVisible = false
            dispose()
        }
        add(abortButton)
        val okButton = JButton("OK")
        okButton.addActionListener(ActionListener {
            if (username.text.isEmpty()) return@ActionListener
            val password = String(password.password)
            callback_.Continue(username.text, password)
            isVisible = false
            dispose()
        })
        add(okButton)
    }
}