// Copyright (c) 2014 The Chromium Embedded Framework Authors. All rights
// reserved. Use of this source code is governed by a BSD-style license that
// can be found in the LICENSE file.
package com.besttech.test.dialog

import org.cef.browser.CefBrowser
import java.awt.BorderLayout
import java.awt.Frame
import java.awt.event.ActionListener
import javax.swing.*

class SearchDialog(owner: Frame?, private val browser_: CefBrowser) : JDialog(owner, "Find...", false) {
    private val searchField = JTextField(30)
    private val caseCheckBox = JCheckBox("Case sensitive")
    private val identifier = Math.random().toInt()
    private val prevButton = JButton("Prev")
    private val nextButton = JButton("Next")
    override fun dispose() {
        browser_.stopFinding(true)
        super.dispose()
    }

    init {
        layout = BorderLayout()
        setSize(400, 100)
        val searchPanel = JPanel()
        searchPanel.layout = BoxLayout(searchPanel, BoxLayout.X_AXIS)
        searchPanel.add(Box.createHorizontalStrut(5))
        searchPanel.add(JLabel("Search:"))
        searchPanel.add(searchField)
        val controlPanel = JPanel()
        controlPanel.layout = BoxLayout(controlPanel, BoxLayout.X_AXIS)
        controlPanel.add(Box.createHorizontalStrut(5))
        val searchButton = JButton("Search")
        searchButton.addActionListener(ActionListener {
            if (searchField.text == null || searchField.text.isEmpty()) return@ActionListener
            title = "Find \"" + searchField.text + "\""
            val matchCase = caseCheckBox.isSelected
            browser_.find(identifier, searchField.text, true, matchCase, false)
            prevButton.isEnabled = true
            nextButton.isEnabled = true
        })
        controlPanel.add(searchButton)
        prevButton.addActionListener {
            val matchCase = caseCheckBox.isSelected
            title = "Find \"" + searchField.text + "\""
            browser_.find(identifier, searchField.text, false, matchCase, true)
        }
        prevButton.isEnabled = false
        controlPanel.add(prevButton)
        nextButton.addActionListener {
            val matchCase = caseCheckBox.isSelected
            title = "Find \"" + searchField.text + "\""
            browser_.find(identifier, searchField.text, true, matchCase, true)
        }
        nextButton.isEnabled = false
        controlPanel.add(nextButton)
        controlPanel.add(Box.createHorizontalStrut(50))
        val doneButton = JButton("Done")
        doneButton.addActionListener {
            isVisible = false
            dispose()
        }
        controlPanel.add(doneButton)
        add(searchPanel, BorderLayout.NORTH)
        add(caseCheckBox)
        add(controlPanel, BorderLayout.SOUTH)
    }
}