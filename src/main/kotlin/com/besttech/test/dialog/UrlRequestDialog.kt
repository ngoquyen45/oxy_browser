// Copyright (c) 2014 The Chromium Embedded Framework Authors. All rights
// reserved. Use of this source code is governed by a BSD-style license that
// can be found in the LICENSE file.
package com.besttech.test.dialog

import org.cef.network.CefPostData
import org.cef.network.CefPostDataElement
import org.cef.network.CefRequest
import org.cef.network.CefRequest.CefUrlRequestFlags
import java.awt.*
import java.awt.event.ActionEvent
import java.awt.event.ActionListener
import java.io.File
import java.util.*
import javax.swing.*
import javax.swing.table.AbstractTableModel

class UrlRequestDialog(owner: Frame, title: String?) : JDialog(owner, title, false) {
    private val requestMethods = Vector<JRadioButton>()
    private val requestFlags: MutableMap<JCheckBox, Int> = HashMap()
    private val headerTblModel: TableModel = TableModel(true)
    private val postDataModel: TableModel = TableModel(false)
    private val urlField: JTextField
    private val cookieUrl: JTextField
    private var myOwner: Frame = owner
    private fun createRequest(): CefRequest? {
        val url = urlField.text
        if (url.isEmpty() || url.trim { it <= ' ' }.equals("http://", ignoreCase = true)) {
            SwingUtilities.invokeLater{
                JOptionPane.showMessageDialog(
                    myOwner,
                    "Please specify at least an URL. Otherwise the CefRequest is invalid"
                )
            }
            return null
        }
        val request = CefRequest.create() ?: return null
        var firstPartyForCookie = cookieUrl.text
        if (firstPartyForCookie.isEmpty() || firstPartyForCookie.trim { it <= ' ' }
                .equals("http://", ignoreCase = true)) firstPartyForCookie = url
        var method = "GET"
        for (i in requestMethods.indices) {
            val button = requestMethods[i]
            if (button.isSelected) {
                method = button.text
                break
            }
        }
        var postData: CefPostData? = null
        val postDataRows = postDataModel.rowCount
        if (postDataRows > 0) {
            postData = CefPostData.create()
        } else if (method.equals("POST", ignoreCase = true) || method.equals("PUT", ignoreCase = true)) {
            SwingUtilities.invokeLater {
                JOptionPane.showMessageDialog(
                    myOwner, "The methods POST and PUT require at least one row of data."
                )
            }
            return null
        }
        if (postData != null) {
            for (i in 0 until postDataRows) {
                val value = postDataModel.getValueAt(i, 0) as String
                if (value.trim { it <= ' ' }.isEmpty()) continue
                val elem = CefPostDataElement.create()
                if (elem != null) {
                    val f = File(value)
                    if (f.isFile) {
                        elem.setToFile(value)
                    } else {
                        val byteStr = value.toByteArray()
                        elem.setToBytes(byteStr.size, byteStr)
                    }
                    postData.addElement(elem)
                }
            }
        }
        var headerMap: MutableMap<String?, String?>? = null
        val headerRows = headerTblModel.rowCount
        if (headerRows > 0) {
            headerMap = HashMap()
            for (i in 0 until headerRows) {
                val key = headerTblModel.getValueAt(i, 0) as String
                val value = headerTblModel.getValueAt(i, 1) as String
                if (key.trim { it <= ' ' }.isEmpty()) continue
                headerMap[key] = value
            }
        }
        var flags = 0
        val entrySet: Set<Map.Entry<JCheckBox, Int>> = requestFlags.entries
        for (entry: Map.Entry<JCheckBox, Int> in entrySet) {
            if (entry.key.isSelected) {
                flags = flags or entry.value
            }
        }
        request[url, method, postData] = headerMap
        request.firstPartyForCookies = firstPartyForCookie
        request.flags = flags
        return request
    }

    private fun addRequestMode(
        panel: JPanel, buttonGrp: ButtonGroup, requestMode: String, selected: Boolean
    ) {
        val button = JRadioButton(requestMode, selected)
        buttonGrp.add(button)
        panel.add(button)
        requestMethods.addElement(button)
    }

    private fun addRequestFlag(
        panel: JPanel, flag: String, value: Int, tooltip: String, selected: Boolean
    ) {
        val checkBox = JCheckBox(flag, selected)
        checkBox.toolTipText = tooltip
        panel.add(checkBox)
        requestFlags[checkBox] = value
    }

    private fun createPanelWithTitle(title: String, rows: Int, cols: Int): JPanel {
        val result = JPanel(GridLayout(rows, cols))
        result.border = BorderFactory.createCompoundBorder(
            BorderFactory.createTitledBorder(title),
            BorderFactory.createEmptyBorder(10, 10, 10, 10)
        )
        return result
    }

    private fun createPanelWithTable(title: String, tblModel: TableModel): JPanel {
        val result = JPanel(BorderLayout())
        result.border = BorderFactory.createCompoundBorder(
            BorderFactory.createTitledBorder(title),
            BorderFactory.createEmptyBorder(10, 10, 10, 10)
        )
        val table = JTable(tblModel)
        table.fillsViewportHeight = true
        val scrollPane = JScrollPane(table)
        val buttonPane = JPanel(GridLayout(0, 2))
        val addButton = JButton("Add entry")
        addButton.addActionListener { tblModel.newDefaultEntry() }
        buttonPane.add(addButton)
        val delButton = JButton("Remove entry")
        delButton.addActionListener { tblModel.removeSelected() }
        buttonPane.add(delButton)
        result.add(scrollPane, BorderLayout.CENTER)
        result.add(buttonPane, BorderLayout.PAGE_END)
        return result
    }

    private inner class TableModel(private val hasKeyColumn: Boolean) : AbstractTableModel() {
        private val columnNames: Array<String> = if (hasKeyColumn) arrayOf("Key", "Value", "") else arrayOf("Value", "")
        private val rowData = Vector<Array<Any>>()
        fun newDefaultEntry() {
            val row = rowData.size
            if (hasKeyColumn) {
                val rowEntry = arrayOf<Any>("key", "value", false)
                rowData.addElement(rowEntry)
            } else {
                val rowEntry = arrayOf<Any>("value", false)
                rowData.addElement(rowEntry)
            }
            fireTableRowsInserted(row, row)
        }

        fun removeSelected() {
            val idx = if (hasKeyColumn) 2 else 1
            var i = 0
            while (i < rowData.size) {
                if (rowData[i][idx] as Boolean) {
                    rowData.removeAt(i)
                    fireTableRowsDeleted(i, i)
                    i--
                }
                ++i
            }
        }

        fun addEntry(key: String, value: String) {
            val row = rowData.size
            if (hasKeyColumn) {
                val rowEntry = arrayOf<Any>(key, value, false)
                rowData.addElement(rowEntry)
            } else {
                val rowEntry = arrayOf<Any>(value, false)
                rowData.addElement(rowEntry)
            }
            fireTableRowsInserted(row, row)
        }

        override fun getRowCount(): Int {
            return rowData.size
        }

        override fun getColumnCount(): Int {
            return columnNames.size
        }

        override fun getColumnName(column: Int): String {
            return columnNames[column]
        }

        override fun getColumnClass(columnIndex: Int): Class<*> {
            return if (rowData.size > 0) rowData[0][columnIndex].javaClass else Any::class.java
        }

        override fun isCellEditable(rowIndex: Int, columnIndex: Int): Boolean {
            return true
        }

        override fun getValueAt(rowIndex: Int, columnIndex: Int): Any {
            return rowData[rowIndex][columnIndex]
        }

        override fun setValueAt(aValue: Any, rowIndex: Int, columnIndex: Int) {
            rowData[rowIndex][columnIndex] = aValue
            fireTableCellUpdated(rowIndex, columnIndex)
        }

    }

    init {
        setSize(1200, 600)
        layout = BorderLayout()

        // URL for the request
        urlField = JTextField("http://")
        val urlPanel = createPanelWithTitle("Request URL", 1, 0)
        urlPanel.add(urlField)

        // URL for the cookies
        cookieUrl = JTextField("http://")
        val cookiePanel = createPanelWithTitle("Cookie-URL", 1, 0)
        cookiePanel.add(cookieUrl)

        // Radio buttons for the request method
        val requestModeGrp = ButtonGroup()
        val requestModePanel = createPanelWithTitle("HTTP Request Mode", 1, 0)
        addRequestMode(requestModePanel, requestModeGrp, "GET", true)
        addRequestMode(requestModePanel, requestModeGrp, "HEAD", false)
        addRequestMode(requestModePanel, requestModeGrp, "POST", false)
        addRequestMode(requestModePanel, requestModeGrp, "PUT", false)
        addRequestMode(requestModePanel, requestModeGrp, "DELETE", false)

        // Checkboxes for the flags
        val flagsPanel = createPanelWithTitle("Flags", 0, 1)
        addRequestFlag(
            flagsPanel, "Skip cache", CefUrlRequestFlags.UR_FLAG_SKIP_CACHE,
            "If set the cache will be skipped when handling the request", false
        )
        addRequestFlag(
            flagsPanel, "Allow cached credentials",
            CefUrlRequestFlags.UR_FLAG_ALLOW_CACHED_CREDENTIALS, (
                    "If set user name, password, and cookies may be sent with the request, "
                            + "and cookies may be saved from the response."),
            false
        )
        addRequestFlag(
            flagsPanel, "Report Upload Progress",
            CefUrlRequestFlags.UR_FLAG_REPORT_UPLOAD_PROGRESS,
            "If set upload progress events will be generated when a request has a body", false
        )
        addRequestFlag(
            flagsPanel, "Report RawHeaders",
            CefUrlRequestFlags.UR_FLAG_REPORT_RAW_HEADERS,
            "If set the headers sent and received for the request will be recorded", false
        )
        addRequestFlag(
            flagsPanel, "No download data", CefUrlRequestFlags.UR_FLAG_NO_DOWNLOAD_DATA,
            "If set the CefURLRequestClient.onDownloadData method will not be called", false
        )
        addRequestFlag(
            flagsPanel, "No retry on 5xx", CefUrlRequestFlags.UR_FLAG_NO_RETRY_ON_5XX,
            "If set 5XX redirect errors will be propagated to the observer instead of automatically re-tried.",
            false
        )

        // Table for header values
        val headerValues = createPanelWithTable("Header Values", headerTblModel)
        headerTblModel.addEntry("User-Agent", "Mozilla/5.0 JCEF Example Agent")
        headerTblModel.addEntry(
            "Accept",
            "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8"
        )

        // Table for post-data
        val postData = createPanelWithTable("Post Data", postDataModel)

        // Create view
        val headerPanel = JPanel(GridBagLayout())
        val c = GridBagConstraints()
        c.fill = GridBagConstraints.HORIZONTAL
        c.weightx = 0.5
        c.gridx = 0
        c.gridy = 0
        headerPanel.add(urlPanel, c)
        c.gridx = 1
        headerPanel.add(cookiePanel, c)
        c.gridx = 0
        c.gridy = 1
        c.gridwidth = 2
        c.weightx = 0.0
        headerPanel.add(requestModePanel, c)
        val centerPanel = JPanel(GridLayout(2, 0))
        centerPanel.add(headerValues)
        centerPanel.add(postData)
        val abortButton = JButton("Abort")
        abortButton.addActionListener {
            isVisible = false
            dispose()
        }
        val sendButton = JButton("Send")
        sendButton.addActionListener(object : ActionListener {
            override fun actionPerformed(e: ActionEvent) {
                val request = createRequest() ?: return
                val handleRequest = UrlRequestDialogReply(myOwner, getTitle() + " - Result")
                handleRequest.send(request)
                handleRequest.isVisible = true
                isVisible = false
                dispose()
            }
        })
        val bottomPanel = JPanel(GridLayout(0, 2))
        bottomPanel.add(abortButton)
        bottomPanel.add(sendButton)
        add(headerPanel, BorderLayout.PAGE_START)
        add(flagsPanel, BorderLayout.LINE_START)
        add(centerPanel, BorderLayout.CENTER)
        add(bottomPanel, BorderLayout.PAGE_END)
    }
}