// Copyright (c) 2014 The Chromium Embedded Framework Authors. All rights
// reserved. Use of this source code is governed by a BSD-style license that
// can be found in the LICENSE file.
package com.besttech.test.dialog

import org.cef.browser.CefBrowser
import java.awt.BorderLayout
import java.awt.Frame
import java.awt.Point
import java.awt.event.ComponentAdapter
import java.awt.event.ComponentEvent
import javax.swing.JDialog

class DevToolsDialog @JvmOverloads constructor(
    owner: Frame,
    title: String?,
    browser: CefBrowser,
    inspectAt: Point? = null
) : JDialog(owner, title, false) {
    private val devTools: CefBrowser
    override fun dispose() {
        devTools.close(true)
        super.dispose()
    }

    init {
        layout = BorderLayout()
        setSize(800, 600)
        setLocation(owner.location.x + 20, owner.location.y + 20)
        devTools = browser.getDevTools(inspectAt)
        add(devTools.uiComponent)
        addComponentListener(object : ComponentAdapter() {
            override fun componentHidden(e: ComponentEvent) {
                dispose()
            }
        })
    }
}