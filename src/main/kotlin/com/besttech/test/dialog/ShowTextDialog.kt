// Copyright (c) 2014 The Chromium Embedded Framework Authors. All rights
// reserved. Use of this source code is governed by a BSD-style license that
// can be found in the LICENSE file.
package com.besttech.test.dialog

import org.cef.callback.CefStringVisitor
import java.awt.BorderLayout
import java.awt.Frame
import javax.swing.*

class ShowTextDialog(owner: Frame?, title: String?) : JDialog(owner, title, false), CefStringVisitor {
    private val textArea_ = JTextArea()
    override fun visit(string: String) {
        if (!isVisible) {
            isVisible = true
        }
        textArea_.append(string)
    }

    init {
        layout = BorderLayout()
        setSize(800, 600)
        val controlPanel = JPanel()
        controlPanel.layout = BoxLayout(controlPanel, BoxLayout.X_AXIS)
        val doneButton = JButton("Done")
        doneButton.addActionListener {
            isVisible = false
            dispose()
        }
        controlPanel.add(doneButton)
        add(JScrollPane(textArea_))
        add(controlPanel, BorderLayout.SOUTH)
    }
}