// Copyright (c) 2014 The Chromium Embedded Framework Authors. All rights
// reserved. Use of this source code is governed by a BSD-style license that
// can be found in the LICENSE file.
package com.besttech.test.dialog

import org.cef.callback.CefAuthCallback
import org.cef.callback.CefURLRequestClient
import org.cef.network.CefRequest
import org.cef.network.CefURLRequest
import java.awt.BorderLayout
import java.awt.Frame
import java.awt.GridLayout
import java.io.ByteArrayOutputStream
import java.io.UnsupportedEncodingException
import javax.swing.*

class UrlRequestDialogReply(owner: Frame, title: String?) : JDialog(owner, title, false), CefURLRequestClient {
    private val myOwner: Frame = owner
    private var nativeRef = 0L
    private val statusLabel = JLabel("HTTP-Request status: ")
    private val sentRequest = JTextArea()
    private val repliedResult = JTextArea()
    private val cancelButton = JButton("Cancel")
    private var urlRequest: CefURLRequest? = null
    private val byteStream = ByteArrayOutputStream()
    private fun createPanelWithTitle(title: String, rows: Int, cols: Int): JPanel {
        val result = JPanel(GridLayout(rows, cols))
        result.border = BorderFactory.createCompoundBorder(
            BorderFactory.createTitledBorder(title),
            BorderFactory.createEmptyBorder(10, 10, 10, 10)
        )
        return result
    }

    fun send(request: CefRequest?) {
        if (request == null) {
            statusLabel.text = "HTTP-Request status: FAILED"
            sentRequest.append("Can't send CefRequest because it is NULL")
            cancelButton.isEnabled = false
            return
        }
        urlRequest = CefURLRequest.create(request, this)
        if (urlRequest == null) {
            statusLabel.text = "HTTP-Request status: FAILED"
            sentRequest.append("Can't send CefRequest because creation of CefURLRequest failed.")
            repliedResult.append(
                "The native code (CEF) returned a NULL-Pointer for CefURLRequest."
            )
            cancelButton.isEnabled = false
        } else {
            sentRequest.append(request.toString())
            cancelButton.isEnabled = true
            updateStatus("", false)
        }
    }

    private fun updateStatus(updateMsg: String, printByteStream: Boolean) {
        val status = urlRequest!!.requestStatus
        val runnable = Runnable {
            statusLabel.text = "HTTP-Request status: $status"
            if (status != CefURLRequest.Status.UR_UNKNOWN && status != CefURLRequest.Status.UR_IO_PENDING) {
                cancelButton.isEnabled = false
            }
            repliedResult.append(updateMsg)
            if (printByteStream) {
                try {
                    repliedResult.append(
                        """
    
    
    ${byteStream.toString("UTF-8")}
    """.trimIndent()
                    )
                } catch (e: UnsupportedEncodingException) {
                    repliedResult.append(
                        """
    
    
    $byteStream
    """.trimIndent()
                    )
                }
            }
        }
        if (SwingUtilities.isEventDispatchThread()) {
            runnable.run()
        } else {
            SwingUtilities.invokeLater(runnable)
        }
    }

    // CefURLRequestClient
    override fun setNativeRef(identifer: String, nativeRef: Long) {
        this.nativeRef = nativeRef
    }

    override fun getNativeRef(identifer: String): Long {
        return nativeRef
    }

    override fun onRequestComplete(request: CefURLRequest) {
        var updateStr = "onRequestCompleted\n\n"
        val response = request.response
        val isText = response.getHeaderByName("Content-Type").startsWith("text")
        updateStr += response.toString()
        updateStatus(updateStr, isText)
    }

    override fun onUploadProgress(request: CefURLRequest, current: Int, total: Int) {
        updateStatus("onUploadProgress: $current/$total bytes\n", false)
    }

    override fun onDownloadProgress(request: CefURLRequest, current: Int, total: Int) {
        updateStatus("onDownloadProgress: $current/$total bytes\n", false)
    }

    override fun onDownloadData(request: CefURLRequest, data: ByteArray, data_length: Int) {
        byteStream.write(data, 0, data_length)
        updateStatus("onDownloadData: $data_length bytes\n", false)
    }

    override fun getAuthCredentials(
        isProxy: Boolean, host: String, port: Int, realm: String,
        scheme: String, callback: CefAuthCallback
    ): Boolean {
        SwingUtilities.invokeLater(PasswordDialog(myOwner, callback))
        return true
    }

    init {
        layout = BorderLayout()
        setSize(800, 600)
        val controlPanel = JPanel()
        controlPanel.layout = BoxLayout(controlPanel, BoxLayout.X_AXIS)
        val doneButton = JButton("Done")
        doneButton.addActionListener {
            urlRequest!!.dispose()
            isVisible = false
            dispose()
        }
        controlPanel.add(doneButton)
        cancelButton.addActionListener {
            if (urlRequest != null) {
                urlRequest!!.cancel()
            }
        }
        cancelButton.isEnabled = false
        controlPanel.add(cancelButton)
        val requestPane = createPanelWithTitle("Sent HTTP-Request", 1, 0)
        requestPane.add(JScrollPane(sentRequest))
        val replyPane = createPanelWithTitle("Reply from the server", 1, 0)
        replyPane.add(JScrollPane(repliedResult))
        val contentPane = JPanel(GridLayout(2, 0))
        contentPane.add(requestPane)
        contentPane.add(replyPane)
        add(statusLabel, BorderLayout.PAGE_START)
        add(contentPane, BorderLayout.CENTER)
        add(controlPanel, BorderLayout.PAGE_END)
    }
}