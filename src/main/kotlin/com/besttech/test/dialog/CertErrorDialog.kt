// Copyright (c) 2014 The Chromium Embedded Framework Authors. All rights
// reserved. Use of this source code is governed by a BSD-style license that
// can be found in the LICENSE file.
package com.besttech.test.dialog

import org.cef.callback.CefRequestCallback
import org.cef.handler.CefLoadHandler
import java.awt.Frame
import javax.swing.JOptionPane

class CertErrorDialog(
    private val owner_: Frame,
    private val cert_error_: CefLoadHandler.ErrorCode,
    private val request_url_: String,
    private val callback_: CefRequestCallback
) : Runnable {
    override fun run() {
        val dialogResult = JOptionPane.showConfirmDialog(
            owner_,
            """
                  An certificate error ($cert_error_) occurreed while requesting
                  $request_url_
                  Do you want to proceed anyway?
                  """.trimIndent(),
            "Certificate error", JOptionPane.YES_NO_OPTION, JOptionPane.ERROR_MESSAGE
        )
        callback_.Continue(dialogResult == JOptionPane.YES_OPTION)
    }
}