// Copyright (c) 2014 The Chromium Embedded Framework Authors. All rights
// reserved. Use of this source code is governed by a BSD-style license that
// can be found in the LICENSE file.
package com.besttech.test.dialog

import org.cef.callback.CefCookieVisitor
import org.cef.misc.BoolRef
import org.cef.network.CefCookie
import org.cef.network.CefCookieManager
import java.awt.BorderLayout
import java.awt.Frame
import java.util.*
import javax.swing.*
import javax.swing.table.AbstractTableModel

class CookieManagerDialog(owner: Frame?, title: String?, cookieManager: CefCookieManager?) :
    JDialog(owner, title, false) {
    private val manager: CefCookieManager?
    private val tblModel: CookieTableModel = CookieTableModel()

    private inner class CookieTableModel : AbstractTableModel(), CefCookieVisitor {
        private val columnNames: Array<String> = arrayOf(
            "Name", "Value", "Domain", "Path", "Secure", "HTTP only",
            "Created", "Last Access", "Expires"
        )
        private val rowData = Vector<Array<Any>>()

        // add an entry to the table
        override fun visit(cookie: CefCookie, count: Int, total: Int, delete: BoolRef): Boolean {
            val entry = arrayOf<Any>(
                cookie.name, cookie.value, cookie.domain, cookie.path, cookie.secure, cookie.httponly, cookie.creation,
                cookie.lastAccess, cookie.expires
            )
            val row = rowData.size
            rowData.addElement(entry)
            fireTableRowsInserted(row, row)
            return true
        }

        fun removeCookies() {
            val cnt = rowData.size
            if (cnt > 0) {
                rowData.clear()
                manager!!.deleteCookies("", "")
                fireTableRowsDeleted(0, cnt - 1)
            }
        }

        override fun getRowCount(): Int {
            return rowData.size
        }

        override fun getColumnCount(): Int {
            return columnNames.size
        }

        override fun getColumnName(column: Int): String {
            return columnNames[column]
        }

        override fun getColumnClass(columnIndex: Int): Class<*> {
            return if (rowData.size > 0) rowData[0][columnIndex].javaClass else Any::class.java
        }

        override fun isCellEditable(rowIndex: Int, columnIndex: Int): Boolean {
            return false
        }

        override fun getValueAt(rowIndex: Int, columnIndex: Int): Any {
            return rowData[rowIndex][columnIndex]
        }

    }

    companion object {
        private var testCookieId = 1
    }

    init {
        layout = BorderLayout()
        setSize(800, 600)
        manager = cookieManager
        val cookieTable = JTable(tblModel)
        cookieTable.fillsViewportHeight = true
        val controlPanel = JPanel()
        controlPanel.layout = BoxLayout(controlPanel, BoxLayout.X_AXIS)
        val delButton = JButton("Delete cookies")
        delButton.addActionListener { tblModel.removeCookies() }
        controlPanel.add(delButton)
        val testCreateCookie = JButton("Add test cookie")
        testCreateCookie.addActionListener {
            val now = Date()
            val expires = Date(now.time + 86400000)
            val name = "testNo" + testCookieId++
            val cookie = CefCookie(
                name, "testCookie", ".test.cookie", "/", false,
                true, now, now, true, expires
            )
            if (manager!!.setCookie("http://my.test.cookie", cookie)) {
                tblModel.visit(cookie, 1, 1, BoolRef())
            }
        }
        controlPanel.add(testCreateCookie)
        val doneButton = JButton("Done")
        doneButton.addActionListener { isVisible = false }
        controlPanel.add(doneButton)
        add(JScrollPane(cookieTable))
        add(controlPanel, BorderLayout.SOUTH)
        if (manager == null) throw NullPointerException("Cookie manager is null")
        manager.visitAllCookies(tblModel)
    }
}