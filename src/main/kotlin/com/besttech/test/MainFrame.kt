// Copyright (c) 2013 The Chromium Embedded Framework Authors. All rights
// reserved. Use of this source code is governed by a BSD-style license that
// can be found in the LICENSE file.
package com.besttech.test


import com.besttech.test.dialog.DownloadDialog
import com.besttech.test.handler.*
import com.besttech.test.ui.ControlPanel
import com.besttech.test.ui.MenuBar
import com.besttech.test.ui.StatusPanel
import com.besttech.test.util.DataUri.Companion.create
import org.cef.CefApp
import org.cef.CefSettings
import org.cef.browser.CefBrowser
import org.cef.browser.CefFrame
import org.cef.browser.CefMessageRouter
import org.cef.handler.CefDisplayHandlerAdapter
import org.cef.handler.CefFocusHandlerAdapter
import org.cef.handler.CefLoadHandler
import org.cef.handler.CefLoadHandlerAdapter
import org.cef.network.CefCookieManager
import java.awt.BorderLayout
import java.awt.KeyboardFocusManager
import java.awt.event.FocusAdapter
import java.awt.event.FocusEvent
import javax.swing.JPanel

class MainFrame(
    osrEnabled: Boolean, transparentPaintingEnabled: Boolean,
    createImmediately: Boolean, myArgs: Array<String>
) : BrowserFrame() {
    private var args = myArgs
    private var errorMsg = ""
    private var controlPane: ControlPanel? = null
    private var statusPanel: StatusPanel? = null
    private var browserFocus = true
    private fun createContentPanel(): JPanel {
        val contentPanel = JPanel(BorderLayout())
        controlPane = ControlPanel(browser)
        statusPanel = StatusPanel()
        contentPanel.add(controlPane, BorderLayout.NORTH)
        contentPanel.add(statusPanel, BorderLayout.SOUTH)
        return contentPanel
    }

    companion object {
        private const val serialVersionUID = -2295538706810864538L
        @JvmStatic
        fun main(args: Array<String>) {
            // Perform startup initialization on platforms that require it.
            if (!CefApp.startup(args)) {
                println("Startup initialization failed!")
                return
            }

            // OSR mode is enabled by default on Linux.
            // and disabled by default on Windows and Mac OS X.
            var osrEnabledArg = false
            var transparentPaintingEnabledArg = false
            var createImmediately = false
            for (arg in args) {
                when (arg.toLowerCase()) {
                    "--off-screen-rendering-enabled" -> osrEnabledArg = true
                    "--transparent-painting-enabled" -> transparentPaintingEnabledArg = true
                    "--create-immediately" -> createImmediately = true
                }
            }
            println("Offscreen rendering " + if (osrEnabledArg) "enabled" else "disabled")

            // MainFrame keeps all the knowledge to display the embedded browser
            // frame.
            val frame = MainFrame(
                osrEnabledArg, transparentPaintingEnabledArg, createImmediately, args
            )
            frame.setSize(800, 600)
            frame.isVisible = true
        }
    }

    init {
        val myApp: CefApp
        if (CefApp.getState() != CefApp.CefAppState.INITIALIZED) {
            // 1) CefApp is the entry point for JCEF. You can pass
            //    application arguments to it, if you want to handle any
            //    chromium or CEF related switches/attributes in
            //    the native world.
            val settings = CefSettings()
            settings.windowless_rendering_enabled = osrEnabled
            // try to load URL "about:blank" to see the background color
            settings.background_color = settings.ColorType(100, 255, 242, 211)
            myApp = CefApp.getInstance(args, settings)
            val version = myApp.version
            println("Using:\n$version")

            //    We're registering our own AppHandler because we want to
            //    add an own schemes (search:// and client://) and its corresponding
            //    protocol handlers. So if you enter "search:something on the web", your
            //    search request "something on the web" is forwarded to www.google.com
            CefApp.addAppHandler(AppHandler(args))
        } else {
            myApp = CefApp.getInstance()
        }

        //    By calling the method createClient() the native part
        //    of JCEF/CEF will be initialized and an  instance of
        //    CefClient will be created. You can create one to many
        //    instances of CefClient.
        val client = myApp.createClient()

        // 2) You have the ability to pass different handlers to your
        //    instance of CefClient. Each handler is responsible to
        //    deal with different informations (e.g. keyboard input).
        //
        //    For each handler (with more than one method) adapter
        //    classes exists. So you don't need to override methods
        //    you're not interested in.
        val downloadDialog = DownloadDialog(this)
        client.addContextMenuHandler(ContextMenuHandler(this))
        client.addDownloadHandler(downloadDialog)
        client.addDragHandler(DragHandler())
        client.addJSDialogHandler(JSDialogHandler())
        client.addKeyboardHandler(KeyboardHandler())
        client.addRequestHandler(RequestHandler(this))

        //    Beside the normal handler instances, we're registering a MessageRouter
        //    as well. That gives us the opportunity to reply to JavaScript method
        //    calls (JavaScript binding). We're using the default configuration, so
        //    that the JavaScript binding methods "cefQuery" and "cefQueryCancel"
        //    are used.
        val msgRouter = CefMessageRouter.create()
        msgRouter.addHandler(MessageRouterHandler(), true)
        msgRouter.addHandler(MessageRouterHandlerEx(client), false)
        client.addMessageRouter(msgRouter)

        // 2.1) We're overriding CefDisplayHandler as nested anonymous class
        //      to update our address-field, the title of the panel as well
        //      as for updating the status-bar on the bottom of the browser
        client.addDisplayHandler(object : CefDisplayHandlerAdapter() {
            override fun onAddressChange(browser: CefBrowser, frame: CefFrame, url: String) {
                controlPane!!.setAddress(browser, url)
            }

            override fun onTitleChange(browser: CefBrowser, title: String) {
                setTitle(title)
            }

            override fun onStatusMessage(browser: CefBrowser, value: String) {
                statusPanel!!.setStatusText(value)
            }
        })

        // 2.2) To disable/enable navigation buttons and to display a prgress bar
        //      which indicates the load state of our website, we're overloading
        //      the CefLoadHandler as nested anonymous class. Beside this, the
        //      load handler is responsible to deal with (load) errors as well.
        //      For example if you navigate to a URL which does not exist, the
        //      browser will show up an error message.
        client.addLoadHandler(object : CefLoadHandlerAdapter() {
            override fun onLoadingStateChange(
                browser: CefBrowser, isLoading: Boolean,
                canGoBack: Boolean, canGoForward: Boolean
            ) {
                controlPane!!.update(browser, isLoading, canGoBack, canGoForward)
                statusPanel!!.setIsInProgress(isLoading)
                if (!isLoading && errorMsg.isNotEmpty()) {
                    browser.loadURL(create("text/html", errorMsg))
                    errorMsg = ""
                }
            }

            override fun onLoadError(
                browser: CefBrowser, frame: CefFrame, errorCode: CefLoadHandler.ErrorCode,
                errorText: String, failedUrl: String
            ) {
                if (errorCode != CefLoadHandler.ErrorCode.ERR_NONE && errorCode != CefLoadHandler.ErrorCode.ERR_ABORTED) {
                    errorMsg = "<html><head>"
                    errorMsg += "<title>Error while loading</title>"
                    errorMsg += "</head><body>"
                    errorMsg += "<h1>$errorCode</h1>"
                    errorMsg += "<h3>Failed to load $failedUrl</h3>"
                    errorMsg += "<p>$errorText</p>"
                    errorMsg += "</body></html>"
                    browser.stopLoad()
                }
            }
        })

        // Create the browser.
        val browser = client.createBrowser(
            "https://www.youtube.com/watch?v=KgOtLOUdCMQ", osrEnabled, transparentPaintingEnabled, null
        )
        this.browser = browser

        // Set up the UI for this example implementation.
        val contentPanel = createContentPanel()
        contentPane.add(contentPanel, BorderLayout.CENTER)

        // Clear focus from the browser when the address field gains focus.
        controlPane!!.addressField.addFocusListener(object : FocusAdapter() {
            override fun focusGained(e: FocusEvent) {
                if (!browserFocus) return
                browserFocus = false
                KeyboardFocusManager.getCurrentKeyboardFocusManager().clearGlobalFocusOwner()
                controlPane!!.addressField.requestFocus()
            }
        })

        // Clear focus from the address field when the browser gains focus.
        client.addFocusHandler(object : CefFocusHandlerAdapter() {
            override fun onGotFocus(browser: CefBrowser) {
                if (browserFocus) return
                browserFocus = true
                KeyboardFocusManager.getCurrentKeyboardFocusManager().clearGlobalFocusOwner()
                browser.setFocus(true)
            }

            override fun onTakeFocus(browser: CefBrowser, next: Boolean) {
                browserFocus = false
            }
        })
        if (createImmediately) browser.createImmediately()

        // Add the browser to the UI.
        contentPanel.add(browser.uiComponent, BorderLayout.CENTER)

        val menuBar = MenuBar(
                this, browser, controlPane!!, downloadDialog, CefCookieManager.getGlobalManager())

        menuBar.addBookmark("Binding Test", "client://tests/binding_test.html")
        menuBar.addBookmark("Binding Test 2", "client://tests/binding_test2.html")
        menuBar.addBookmark("Download Test", "http://opensource.spotify.com/cefbuilds/index.html")
        menuBar.addBookmark("Login Test (username:pumpkin, password:pie)",
                "http://www.colostate.edu/~ric/protect/your.html")
        menuBar.addBookmark("Certificate-error Test", "https://www.k2go.de")
        menuBar.addBookmark("Resource-Handler Test", "http://www.foo.bar/")
        menuBar.addBookmark("Resource-Handler Set Error Test", "http://seterror.test/")
        menuBar.addBookmark(
                "Scheme-Handler Test 1: (scheme \"client\")", "client://tests/handler.html")
        menuBar.addBookmark(
                "Scheme-Handler Test 2: (scheme \"search\")", "search://do a barrel roll/")
        menuBar.addBookmark("Spellcheck Test", "client://tests/spellcheck.html")
        menuBar.addBookmark("LocalStorage Test", "client://tests/localstorage.html")
        menuBar.addBookmark("Transparency Test", "client://tests/transparency.html")
        menuBar.addBookmarkSeparator()
        menuBar.addBookmark(
                "javachromiumembedded", "https://bitbucket.org/chromiumembedded/java-cef")
        menuBar.addBookmark("chromiumembedded", "https://bitbucket.org/chromiumembedded/cef")
        jMenuBar = menuBar
    }
}