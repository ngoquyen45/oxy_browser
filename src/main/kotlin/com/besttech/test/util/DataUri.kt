// Copyright (c) 2019 The Chromium Embedded Framework Authors. All rights
// reserved. Use of this source code is governed by a BSD-style license that
// can be found in the LICENSE file.
package com.besttech.test.util

import java.util.*

/**
 * Utility class for creating data: URIs that can be passed to CefBrowser.loadURL.
 */

class DataUri {
    companion object {
        @JvmStatic
        fun create(mimeType: String, contents: String): String {
            return ("data:" + mimeType + ";base64,"
                    + Base64.getEncoder().encodeToString(contents.toByteArray()))
        }
    }
}
