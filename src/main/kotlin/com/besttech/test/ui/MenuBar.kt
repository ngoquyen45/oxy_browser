// Copyright (c) 2014 The Chromium Embedded Framework Authors. All rights
// reserved. Use of this source code is governed by a BSD-style license that
// can be found in the LICENSE file.
package com.besttech.test.ui

import com.besttech.test.BrowserFrame
import com.besttech.test.MainFrame
import com.besttech.test.dialog.*
import com.besttech.test.util.DataUri
import org.cef.OS
import org.cef.browser.CefBrowser
import org.cef.callback.CefRunFileDialogCallback
import org.cef.callback.CefStringVisitor
import org.cef.handler.CefDialogHandler.FileDialogMode
import org.cef.misc.CefPdfPrintSettings
import org.cef.network.CefCookieManager
import org.cef.network.CefRequest
import java.awt.BorderLayout
import java.awt.event.ActionListener
import java.awt.event.ComponentAdapter
import java.awt.event.ComponentEvent
import java.awt.event.WindowEvent
import java.io.File
import java.io.FileNotFoundException
import java.io.PrintWriter
import java.io.UnsupportedEncodingException
import javax.swing.*

class MenuBar(
    private val owner: BrowserFrame, browser: CefBrowser, control_pane: ControlPanel,
    downloadDialog: DownloadDialog, cookieManager: CefCookieManager
) : JMenuBar() {
    internal inner class SaveAs(fName: String?) : CefStringVisitor {
        private val printWriter: PrintWriter = PrintWriter(fName ?: "", "UTF-8")
        override fun visit(string: String) {
            printWriter.write(string)
            printWriter.close()
        }

    }

    private val browser_: CefBrowser?
    private var last_selected_file_ = ""
    private val bookmarkMenu_: JMenu?
    private val control_pane_: ControlPanel
    private val downloadDialog_: DownloadDialog
    private val cookieManager: CefCookieManager
    private var reparentPending_ = false
    fun addBookmark(name: String, URL: String?) {
        if (bookmarkMenu_ == null) return

        // Test if the bookmark already exists. If yes, update URL
        val entries = bookmarkMenu_.menuComponents
        for (itemEntry in entries) {
            if (itemEntry !is JMenuItem) continue
            val item = itemEntry
            if (item.text == name) {
                item.actionCommand = URL
                return
            }
        }
        val menuItem = JMenuItem(name)
        menuItem.actionCommand = URL
        menuItem.addActionListener { e -> browser_!!.loadURL(e.actionCommand) }
        bookmarkMenu_.add(menuItem)
        validate()
    }

    fun addBookmarkSeparator() {
        bookmarkMenu_!!.addSeparator()
    }

    init {
        browser_ = browser
        control_pane_ = control_pane
        downloadDialog_ = downloadDialog
        this.cookieManager = cookieManager
        isEnabled = browser_ != null
        val fileMenu = JMenu("File")
        val openFileItem = JMenuItem("Open file...")
        openFileItem.addActionListener {
            val fc = JFileChooser(File(last_selected_file_))
            // Show open dialog; this method does not return until the dialog is closed.
            fc.showOpenDialog(owner)
            val selectedFile = fc.selectedFile
            if (selectedFile != null) {
                last_selected_file_ = selectedFile.absolutePath
                browser_.loadURL("file:///" + selectedFile.absolutePath)
            }
        }
        fileMenu.add(openFileItem)
        val openFileDialog = JMenuItem("Save as...")
        openFileDialog.addActionListener {
            val callback = CefRunFileDialogCallback { selectedAcceptFilter, filePaths ->
                if (!filePaths.isEmpty()) {
                    try {
                        val saveContent: SaveAs = SaveAs(
                            filePaths[0]
                        )
                        browser_.getSource(saveContent)
                    } catch (e: FileNotFoundException) {
                        browser_.executeJavaScript(
                            "alert(\"Can't save file\");",
                            control_pane_.address, 0
                        )
                    } catch (e: UnsupportedEncodingException) {
                        browser_.executeJavaScript(
                            "alert(\"Can't save file\");",
                            control_pane_.address, 0
                        )
                    }
                }
            }
            browser_.runFileDialog(
                FileDialogMode.FILE_DIALOG_SAVE, owner.title,
                "index.html", null, 0, callback
            )
        }
        fileMenu.add(openFileDialog)
        val printItem = JMenuItem("Print...")
        printItem.addActionListener { browser_.print() }
        fileMenu.add(printItem)
        val printToPdfItem = JMenuItem("Print to PDF")
        printToPdfItem.addActionListener {
            val fc = JFileChooser()
            fc.showSaveDialog(owner)
            val selectedFile = fc.selectedFile
            if (selectedFile != null) {
                val pdfSettings = CefPdfPrintSettings()
                pdfSettings.header_footer_enabled = true
                // A4 page size
                pdfSettings.page_width = 210000
                pdfSettings.page_height = 297000
                browser.printToPDF(
                    selectedFile.absolutePath, pdfSettings
                ) { path, ok ->
                    SwingUtilities.invokeLater {
                        if (ok) {
                            JOptionPane.showMessageDialog(
                                owner,
                                "PDF saved to $path", "Success",
                                JOptionPane.INFORMATION_MESSAGE
                            )
                        } else {
                            JOptionPane.showMessageDialog(
                                owner, "PDF failed",
                                "Failed", JOptionPane.ERROR_MESSAGE
                            )
                        }
                    }
                }
            }
        }
        fileMenu.add(printToPdfItem)
        val searchItem = JMenuItem("Search...")
        searchItem.addActionListener { SearchDialog(owner, browser_).isVisible = true }
        fileMenu.add(searchItem)
        fileMenu.addSeparator()
        val viewSource = JMenuItem("View source")
        viewSource.addActionListener { browser_.viewSource() }
        fileMenu.add(viewSource)
        val getSource = JMenuItem("Get source...")
        getSource.addActionListener {
            val visitor = ShowTextDialog(
                owner, "Source of \"" + control_pane_.address + "\""
            )
            browser_.getSource(visitor)
        }
        fileMenu.add(getSource)
        val getText = JMenuItem("Get text...")
        getText.addActionListener {
            val visitor = ShowTextDialog(
                owner, "Content of \"" + control_pane_.address + "\""
            )
            browser_.getText(visitor)
        }
        fileMenu.add(getText)
        fileMenu.addSeparator()
        val showDownloads = JMenuItem("Show Downloads")
        showDownloads.addActionListener { downloadDialog_.isVisible = true }
        fileMenu.add(showDownloads)
        val showCookies = JMenuItem("Show Cookies")
        showCookies.addActionListener {
            val cm = CookieManagerDialog(owner, "Cookie Manager", this.cookieManager)
            cm.isVisible = true
        }
        fileMenu.add(showCookies)
        val showPlugins = JMenuItem("Show Plugins")
        showPlugins.addActionListener {
            val pluginManager = WebPluginManagerDialog(owner, "Plugin Manager")
            pluginManager.isVisible = true
        }
        fileMenu.add(showPlugins)
        fileMenu.addSeparator()
        val exitItem = JMenuItem("Exit")
        exitItem.addActionListener { owner.dispatchEvent(WindowEvent(owner, WindowEvent.WINDOW_CLOSING)) }
        fileMenu.add(exitItem)
        bookmarkMenu_ = JMenu("Bookmarks")
        val addBookmarkItem = JMenuItem("Add bookmark")
        addBookmarkItem.addActionListener { addBookmark(owner.title, control_pane_.address) }
        bookmarkMenu_.add(addBookmarkItem)
        bookmarkMenu_.addSeparator()
        val testMenu = JMenu("Tests")
        val testJSItem = JMenuItem("JavaScript alert")
        testJSItem.addActionListener { browser_.executeJavaScript("alert('Hello World');", control_pane_.address, 1) }
        testMenu.add(testJSItem)
        val jsAlertItem = JMenuItem("JavaScript alert (will be suppressed)")
        jsAlertItem.addActionListener {
            browser_.executeJavaScript(
                "alert('Never displayed');",
                "http://dontshow.me",
                1
            )
        }
        testMenu.add(jsAlertItem)
        val testShowText = JMenuItem("Show Text")
        testShowText.addActionListener {
            browser_.loadURL(
                DataUri.create(
                    "text/html", "<html><body><h1>Hello World</h1></body></html>"
                )
            )
        }
        testMenu.add(testShowText)
        val showForm = JMenuItem("RequestHandler Test")
        showForm.addActionListener {
            var form = "<html><head><title>RequestHandler test</title></head>"
            form += "<body><h1>RequestHandler test</h1>"
            form += "<form action=\"http://www.google.com/\" method=\"post\">"
            form += "<input type=\"text\" name=\"searchFor\"/>"
            form += "<input type=\"submit\"/><br/>"
            form += "<input type=\"checkbox\" name=\"sendAsGet\"> Use GET instead of POST"
            form += "<p>This form tries to send the content of the text field as HTTP-POST request to http://www.google.com.</p>"
            form += "<h2>Testcase 1</h2>"
            form += "Try to enter the word <b>\"ignore\"</b> into the text field and press \"submit\".<br />"
            form += "The request will be rejected by the application."
            form += "<p>See implementation of <u>tests.RequestHandler.onBeforeBrowse(CefBrowser, CefRequest, boolean)</u> for details</p>"
            form += "<h2>Testcase 2</h2>"
            form += "Due Google doesn't allow the POST method, the server replies with a 405 error.</br>"
            form += "If you activate the checkbox \"Use GET instead of POST\", the application will change the POST request into a GET request."
            form += "<p>See implementation of <u>tests.RequestHandler.onBeforeResourceLoad(CefBrowser, CefRequest)</u> for details</p>"
            form += "</form>"
            form += "</body></html>"
            browser_.loadURL(DataUri.create("text/html", form))
        }
        testMenu.add(showForm)
        val httpRequest = JMenuItem("Manual HTTP request")
        httpRequest.addActionListener {
            val searchFor = JOptionPane.showInputDialog(owner, "Search on google:")
            if (searchFor != null && !searchFor.isEmpty()) {
                val myRequest = CefRequest.create()
                myRequest.method = "GET"
                myRequest.url = "http://www.google.com/#q=$searchFor"
                myRequest.firstPartyForCookies = "http://www.google.com/#q=$searchFor"
                browser_.loadRequest(myRequest)
            }
        }
        testMenu.add(httpRequest)
        val showInfo = JMenuItem("Show Info")
        showInfo.addActionListener {
            var info = "<html><head><title>Browser status</title></head>"
            info += "<body><h1>Browser status</h1><table border=\"0\">"
            info += "<tr><td>CanGoBack</td><td>" + browser_.canGoBack() + "</td></tr>"
            info += "<tr><td>CanGoForward</td><td>" + browser_.canGoForward() + "</td></tr>"
            info += "<tr><td>IsLoading</td><td>" + browser_.isLoading + "</td></tr>"
            info += "<tr><td>isPopup</td><td>" + browser_.isPopup + "</td></tr>"
            info += "<tr><td>hasDocument</td><td>" + browser_.hasDocument() + "</td></tr>"
            info += "<tr><td>Url</td><td>" + browser_.url + "</td></tr>"
            info += "<tr><td>Zoom-Level</td><td>" + browser_.zoomLevel + "</td></tr>"
            info += "</table></body></html>"
            val js = ("var x=window.open(); x.document.open(); x.document.write('" + info
                    + "'); x.document.close();")
            browser_.executeJavaScript(js, "", 0)
        }
        testMenu.add(showInfo)
        val showDevTools = JMenuItem("Show DevTools")
        showDevTools.addActionListener {
            val devToolsDlg = DevToolsDialog(owner, "DEV Tools", browser_)
            devToolsDlg.addComponentListener(object : ComponentAdapter() {
                override fun componentHidden(e: ComponentEvent) {
                    showDevTools.isEnabled = true
                }
            })
            devToolsDlg.isVisible = true
            showDevTools.isEnabled = false
        }
        testMenu.add(showDevTools)
        val testURLRequest = JMenuItem("URL Request")
        testURLRequest.addActionListener {
            val dlg = UrlRequestDialog(owner, "URL Request Test")
            dlg.isVisible = true
        }
        testMenu.add(testURLRequest)
        val reparent = JMenuItem("Reparent")
        reparent.addActionListener(ActionListener {
            val newFrame = BrowserFrame("New Window")
            newFrame.layout = BorderLayout()
            val reparentButton = JButton("Reparent <")
            reparentButton.addActionListener(ActionListener {
                if (reparentPending_) return@ActionListener
                reparentPending_ = true
                if (reparentButton.text == "Reparent <") {
                    owner.removeBrowser {
                        newFrame.add(browser_.uiComponent, BorderLayout.CENTER)
                        newFrame.browser = browser_
                        reparentButton.text = "Reparent >"
                        reparentPending_ = false
                    }
                } else {
                    newFrame.removeBrowser {
                        val rootPane = owner.getComponent(0) as JRootPane
                        val container = rootPane.contentPane
                        val panel = container.getComponent(0) as JPanel
                        panel.add(browser_.uiComponent)
                        owner.browser = browser_
                        owner.revalidate()
                        reparentButton.text = "Reparent <"
                        reparentPending_ = false
                    }
                }
            })
            newFrame.add(reparentButton, BorderLayout.NORTH)
            newFrame.setSize(400, 400)
            newFrame.isVisible = true
        })
        testMenu.add(reparent)
        val newwindow = JMenuItem("New window")
        newwindow.addActionListener {
            val frame = MainFrame(OS.isLinux(), false, false, emptyArray())
            frame.setSize(800, 600)
            frame.isVisible = true
        }
        testMenu.add(newwindow)
        add(fileMenu)
        add(bookmarkMenu_)
        add(testMenu)
    }
}