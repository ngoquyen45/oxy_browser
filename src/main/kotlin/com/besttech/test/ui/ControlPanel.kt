// Copyright (c) 2014 The Chromium Embedded Framework Authors. All rights
// reserved. Use of this source code is governed by a BSD-style license that
// can be found in the LICENSE file.
package com.besttech.test.ui

import org.cef.OS
import org.cef.browser.CefBrowser
import java.awt.event.ActionEvent
import java.net.URI
import java.net.URISyntaxException
import javax.swing.*

class ControlPanel(browser: CefBrowser?) : JPanel() {
    private val backButton: JButton
    private val forwardButton: JButton
    private val reloadButton: JButton
    val addressField: JTextField
    private val zoomlabel: JLabel
    private var zoomLevel = 0.0
    private val browser: CefBrowser?
    fun update(
        browser: CefBrowser, isLoading: Boolean, canGoBack: Boolean, canGoForward: Boolean
    ) {
        if (browser === this.browser) {
            backButton.isEnabled = canGoBack
            forwardButton.isEnabled = canGoForward
            reloadButton.text = if (isLoading) "Abort" else "Reload"
        }
    }

    // If the URI format is unknown "new URI" will throw an
    // exception. In this case we interpret the value of the
    // address field as search request. Therefore we simply add
    // the "search" scheme.
    val address: String
        get() {
            var address = addressField.text
            // If the URI format is unknown "new URI" will throw an
            // exception. In this case we interpret the value of the
            // address field as search request. Therefore we simply add
            // the "search" scheme.
            try {
                address = address.replace(" ".toRegex(), "%20")
                val test = URI(address)
                if (test.scheme != null) return address
                if (test.host != null && test.path != null) return address
                val specific = test.schemeSpecificPart
                if (specific.indexOf('.') == -1) throw URISyntaxException(specific, "No dot inside domain")
            } catch (e1: URISyntaxException) {
                address = "search://$address"
            }
            return address
        }

    fun setAddress(browser: CefBrowser, address: String?) {
        if (browser === this.browser) addressField.text = address
    }

    init {
        assert(browser != null)
        this.browser = browser
        layout = BoxLayout(this, BoxLayout.X_AXIS)
        add(Box.createHorizontalStrut(5))
        add(Box.createHorizontalStrut(5))
        backButton = JButton("Back")
        backButton.isFocusable = false
        backButton.alignmentX = LEFT_ALIGNMENT
        backButton.addActionListener { this.browser!!.goBack() }
        add(backButton)
        add(Box.createHorizontalStrut(5))
        forwardButton = JButton("Forward")
        forwardButton.isFocusable = false
        forwardButton.alignmentX = LEFT_ALIGNMENT
        forwardButton.addActionListener { this.browser!!.goForward() }
        add(forwardButton)
        add(Box.createHorizontalStrut(5))
        reloadButton = JButton("Reload")
        reloadButton.isFocusable = false
        reloadButton.alignmentX = LEFT_ALIGNMENT
        reloadButton.addActionListener { e ->
            if (reloadButton.text.equals("reload", ignoreCase = true)) {
                val mask = if (OS.isMacintosh()) ActionEvent.META_MASK else ActionEvent.CTRL_MASK
                if (e.modifiers and mask != 0) {
                    println("Reloading - ignoring cached values")
                    this.browser!!.reloadIgnoreCache()
                } else {
                    println("Reloading - using cached values")
                    this.browser!!.reload()
                }
            } else {
                this.browser!!.stopLoad()
            }
        }
        add(reloadButton)
        add(Box.createHorizontalStrut(5))
        val addressLabel = JLabel("Address:")
        addressLabel.alignmentX = LEFT_ALIGNMENT
        add(addressLabel)
        add(Box.createHorizontalStrut(5))
        addressField = JTextField(100)
        addressField.alignmentX = LEFT_ALIGNMENT
        addressField.addActionListener { this.browser!!.loadURL(address) }
        add(addressField)
        add(Box.createHorizontalStrut(5))
        val goButton = JButton("Go")
        goButton.isFocusable = false
        goButton.alignmentX = LEFT_ALIGNMENT
        goButton.addActionListener { this.browser!!.loadURL(address) }
        add(goButton)
        add(Box.createHorizontalStrut(5))

        zoomlabel = JLabel("0.0")
        add(zoomlabel)
        val minusButton = JButton("-")
        minusButton.isFocusable = false
        minusButton.alignmentX = CENTER_ALIGNMENT
        minusButton.addActionListener {
            this.browser!!.zoomLevel = --zoomLevel
            zoomlabel.text = zoomLevel.toString()
        }
        add(minusButton)
        val plusButton = JButton("+")
        plusButton.isFocusable = false
        plusButton.alignmentX = CENTER_ALIGNMENT
        plusButton.addActionListener {
            this.browser!!.zoomLevel = ++zoomLevel
            zoomlabel.text = zoomLevel.toString()
        }
        add(plusButton)
    }
}