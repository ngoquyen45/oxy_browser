// Copyright (c) 2014 The Chromium Embedded Framework Authors. All rights
// reserved. Use of this source code is governed by a BSD-style license that
// can be found in the LICENSE file.
package com.besttech.test.ui

import javax.swing.*

class StatusPanel : JPanel() {
    private val progressBar: JProgressBar
    private val statusField: JLabel
    fun setIsInProgress(inProgress: Boolean) {
        progressBar.isIndeterminate = inProgress
    }

    fun setStatusText(text: String?) {
        statusField.text = text
    }

    init {
        layout = BoxLayout(this, BoxLayout.X_AXIS)
        add(Box.createHorizontalStrut(5))
        add(Box.createHorizontalStrut(5))
        progressBar = JProgressBar()
        val progressBarSize = progressBar.maximumSize
        progressBarSize.width = 100
        progressBar.minimumSize = progressBarSize
        progressBar.maximumSize = progressBarSize
        add(progressBar)
        add(Box.createHorizontalStrut(5))
        statusField = JLabel("Info")
        statusField.alignmentX = LEFT_ALIGNMENT
        add(statusField)
        add(Box.createHorizontalStrut(5))
        add(Box.createVerticalStrut(21))
    }
}