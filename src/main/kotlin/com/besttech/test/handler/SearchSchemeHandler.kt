// Copyright (c) 2014 The Chromium Embedded Framework Authors. All rights
// reserved. Use of this source code is governed by a BSD-style license that
// can be found in the LICENSE file.
package com.besttech.test.handler

import org.cef.browser.CefBrowser
import org.cef.callback.CefCallback
import org.cef.handler.CefResourceHandlerAdapter
import org.cef.network.CefRequest

/**
 * In this case we create a new CefRequest object with
 * http://www.google.com/#q=<entered value without scheme search>
 * as target and forward it to the CefBrowser instance.
 * The "search://"-request is aborted by returning false.
</entered> */
class SearchSchemeHandler(private val browser_: CefBrowser) : CefResourceHandlerAdapter() {
    override fun processRequest(request: CefRequest, callback: CefCallback): Boolean {
        // cut away "scheme://"
        val requestUrl = request.url
        var newUrl = requestUrl.substring(scheme.length + 3)
        // cut away a trailing "/" if any
        if (newUrl.indexOf('/') == newUrl.length - 1) {
            newUrl = newUrl.substring(0, newUrl.length - 1)
        }
        newUrl = "http://www.google.com/#q=$newUrl"
        val newRequest = CefRequest.create()
        if (newRequest != null) {
            newRequest.method = "GET"
            newRequest.url = newUrl
            newRequest.firstPartyForCookies = newUrl
            browser_.loadRequest(newRequest)
        }
        return false
    }

    companion object {
        const val scheme = "search"
        const val domain = ""
    }
}