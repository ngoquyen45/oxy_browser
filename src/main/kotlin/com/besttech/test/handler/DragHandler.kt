// Copyright (c) 2014 The Chromium Embedded Framework Authors. All rights
// reserved. Use of this source code is governed by a BSD-style license that
// can be found in the LICENSE file.
package com.besttech.test.handler

import org.cef.handler.CefDragHandler
import org.cef.browser.CefBrowser
import org.cef.callback.CefDragData
import org.cef.handler.CefDragHandler.DragOperationMask

class DragHandler : CefDragHandler {
    override fun onDragEnter(browser: CefBrowser, dragData: CefDragData, mask: Int): Boolean {
        println("DRAG:")
        print("  flags:")
        if (mask and DragOperationMask.DRAG_OPERATION_COPY != 0) print(" COPY")
        if (mask and DragOperationMask.DRAG_OPERATION_LINK != 0) print(" LINK")
        if (mask and DragOperationMask.DRAG_OPERATION_GENERIC != 0) print(" GENERIC")
        if (mask and DragOperationMask.DRAG_OPERATION_PRIVATE != 0) print(" PRIVATE")
        if (mask and DragOperationMask.DRAG_OPERATION_MOVE != 0) print(" MOVE")
        if (mask and DragOperationMask.DRAG_OPERATION_DELETE != 0) print(" DELETE")
        println("\n  $dragData")
        return false
    }
}