// Copyright (c) 2014 The Chromium Embedded Framework Authors. All rights
// reserved. Use of this source code is governed by a BSD-style license that
// can be found in the LICENSE file.
package com.besttech.test.handler

import org.cef.browser.CefBrowser
import org.cef.callback.CefJSDialogCallback
import org.cef.handler.CefJSDialogHandler.JSDialogType
import org.cef.handler.CefJSDialogHandlerAdapter
import org.cef.misc.BoolRef

class JSDialogHandler : CefJSDialogHandlerAdapter() {
    override fun onJSDialog(
        browser: CefBrowser, origin_url: String, dialog_type: JSDialogType,
        message_text: String, default_prompt_text: String, callback: CefJSDialogCallback,
        suppress_message: BoolRef
    ): Boolean {
        if (message_text.equals("Never displayed", ignoreCase = true)) {
            suppress_message.set(true)
            println(
                "The $dialog_type from origin \"$origin_url\" was suppressed."
            )
            println(
                "   The content of the suppressed dialog was: \"$message_text\""
            )
        }
        return false
    }
}