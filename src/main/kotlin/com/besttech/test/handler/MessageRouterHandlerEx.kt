// Copyright (c) 2014 The Chromium Embedded Framework Authors. All rights
// reserved. Use of this source code is governed by a BSD-style license that
// can be found in the LICENSE file.
package com.besttech.test.handler

import org.cef.CefClient
import org.cef.browser.CefBrowser
import org.cef.browser.CefFrame
import org.cef.browser.CefMessageRouter
import org.cef.browser.CefMessageRouter.CefMessageRouterConfig
import org.cef.callback.CefQueryCallback
import org.cef.handler.CefMessageRouterHandlerAdapter

class MessageRouterHandlerEx(private val client_: CefClient) : CefMessageRouterHandlerAdapter() {
    private val config_ = CefMessageRouterConfig("myQuery", "myQueryAbort")
    private var router_: CefMessageRouter? = null
    override fun onQuery(
        browser: CefBrowser, frame: CefFrame, query_id: Long, request: String,
        persistent: Boolean, callback: CefQueryCallback
    ): Boolean {
        if (request.startsWith("hasExtension")) {
            if (router_ != null) callback.success("") else callback.failure(0, "")
        } else if (request.startsWith("enableExt")) {
            if (router_ != null) {
                callback.failure(-1, "Already enabled")
            } else {
                router_ = CefMessageRouter.create(config_, JavaVersionMessageRouter())
                client_.addMessageRouter(router_)
                callback.success("")
            }
        } else if (request.startsWith("disableExt")) {
            if (router_ == null) {
                callback.failure(-2, "Already disabled")
            } else {
                client_.removeMessageRouter(router_)
                router_!!.dispose()
                router_ = null
                callback.success("")
            }
        } else {
            // not handled
            return false
        }
        return true
    }

    private inner class JavaVersionMessageRouter : CefMessageRouterHandlerAdapter() {
        override fun onQuery(
            browser: CefBrowser, frame: CefFrame, query_id: Long, request: String,
            persistent: Boolean, callback: CefQueryCallback
        ): Boolean {
            if (request.startsWith("jcefJava")) {
                callback.success(System.getProperty("java.version"))
                return true
            }
            return false
        }
    }
}