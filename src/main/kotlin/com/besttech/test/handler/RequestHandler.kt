// Copyright (c) 2014 The Chromium Embedded Framework Authors. All rights
// reserved. Use of this source code is governed by a BSD-style license that
// can be found in the LICENSE file.
package com.besttech.test.handler

import com.besttech.test.dialog.CertErrorDialog
import com.besttech.test.dialog.PasswordDialog
import org.cef.browser.CefBrowser
import org.cef.browser.CefFrame
import org.cef.callback.CefAuthCallback
import org.cef.callback.CefRequestCallback
import org.cef.handler.*
import org.cef.handler.CefRequestHandler.TerminationStatus
import org.cef.misc.BoolRef
import org.cef.network.CefPostDataElement
import org.cef.network.CefRequest
import java.awt.Frame
import java.util.*
import javax.swing.JOptionPane
import javax.swing.SwingUtilities

class RequestHandler(private val owner_: Frame) : CefResourceRequestHandlerAdapter(), CefRequestHandler {
    override fun onBeforeBrowse(
        browser: CefBrowser, frame: CefFrame, request: CefRequest,
        user_gesture: Boolean, is_redirect: Boolean
    ): Boolean {
        val postData = request.postData
        if (postData != null) {
            val elements = Vector<CefPostDataElement>()
            postData.getElements(elements)
            for (el in elements) {
                val numBytes = el.bytesCount
                if (numBytes <= 0) continue
                val readBytes = ByteArray(numBytes)
                if (el.getBytes(numBytes, readBytes) <= 0) continue
                val readString = String(readBytes)
                if (readString.indexOf("ignore") > -1) {
                    SwingUtilities.invokeLater {
                        JOptionPane.showMessageDialog(
                            owner_,
                            "The request was rejected because you've entered \"ignore\" into the form."
                        )
                    }
                    return true
                }
            }
        }
        return false
    }

    override fun getResourceRequestHandler(
        browser: CefBrowser, frame: CefFrame,
        request: CefRequest, isNavigation: Boolean, isDownload: Boolean, requestInitiator: String,
        disableDefaultHandling: BoolRef
    ): CefResourceRequestHandler {
        return this
    }

    override fun onBeforeResourceLoad(browser: CefBrowser, frame: CefFrame, request: CefRequest): Boolean {
        // If you send a HTTP-POST request to http://www.google.com/
        // google rejects your request because they don't allow HTTP-POST.
        //
        // This test extracts the value of the test form.
        // (see "Show Form" entry within BrowserMenuBar)
        // and sends its value as HTTP-GET request to Google.
        if (request.method.equals("POST", ignoreCase = true)
            && request.url == "http://www.google.com/"
        ) {
            var forwardTo = "http://www.google.com/#q="
            val postData = request.postData
            var sendAsGet = false
            if (postData != null) {
                val elements = Vector<CefPostDataElement>()
                postData.getElements(elements)
                for (el in elements) {
                    val numBytes = el.bytesCount
                    if (numBytes <= 0) continue
                    val readBytes = ByteArray(numBytes)
                    if (el.getBytes(numBytes, readBytes) <= 0) continue
                    val readString = String(readBytes).trim { it <= ' ' }
                    val stringPairs = readString.split("&").toTypedArray()
                    for (s in stringPairs) {
                        val startPos = s.indexOf('=')
                        if (s.startsWith("searchFor")) forwardTo += s.substring(startPos + 1) else if (s.startsWith("sendAsGet")) {
                            sendAsGet = true
                        }
                    }
                }
                if (sendAsGet) postData.removeElements()
            }
            if (sendAsGet) {
                request.flags = 0
                request.method = "GET"
                request.url = forwardTo
                request.firstPartyForCookies = forwardTo
                val headerMap = HashMap<String, String>()
                request.getHeaderMap(headerMap)
                headerMap.remove("Content-Type")
                headerMap.remove("Origin")
                request.setHeaderMap(headerMap)
            }
        }
        return false
    }

    override fun getResourceHandler(
        browser: CefBrowser, frame: CefFrame, request: CefRequest
    ): CefResourceHandler? {
        // the non existing domain "foo.bar" is handled by the ResourceHandler implementation
        // E.g. if you try to load the URL http://www.foo.bar, you'll be forwarded
        // to the ResourceHandler class.
        if (request.url.endsWith("foo.bar/")) {
            return ResourceHandler()
        }
        return if (request.url.endsWith("seterror.test/")) {
            ResourceSetErrorHandler()
        } else null
    }

    override fun getAuthCredentials(
        browser: CefBrowser, origin_url: String, isProxy: Boolean,
        host: String, port: Int, realm: String, scheme: String, callback: CefAuthCallback
    ): Boolean {
        SwingUtilities.invokeLater(PasswordDialog(owner_, callback))
        return true
    }

    override fun onQuotaRequest(
        browser: CefBrowser, origin_url: String, new_size: Long, callback: CefRequestCallback
    ): Boolean {
        return false
    }

    override fun onCertificateError(
        browser: CefBrowser, cert_error: CefLoadHandler.ErrorCode, request_url: String,
        callback: CefRequestCallback
    ): Boolean {
        SwingUtilities.invokeLater(CertErrorDialog(owner_, cert_error, request_url, callback))
        return true
    }

    override fun onPluginCrashed(browser: CefBrowser, pluginPath: String) {
        println("Plugin " + pluginPath + "CRASHED")
    }

    override fun onRenderProcessTerminated(browser: CefBrowser, status: TerminationStatus) {
        println("render process terminated: $status")
    }
}