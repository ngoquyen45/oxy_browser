// Copyright (c) 2014 The Chromium Embedded Framework Authors. All rights
// reserved. Use of this source code is governed by a BSD-style license that
// can be found in the LICENSE file.
package com.besttech.test.handler

import com.besttech.test.dialog.SearchDialog
import com.besttech.test.dialog.ShowTextDialog
import org.cef.browser.CefBrowser
import org.cef.browser.CefFrame
import org.cef.callback.CefContextMenuParams
import org.cef.callback.CefMenuModel
import org.cef.handler.CefContextMenuHandler
import java.awt.Frame
import java.util.*
import kotlin.collections.HashMap

class ContextMenuHandler(private val owner_: Frame) : CefContextMenuHandler {
    private val suggestions: MutableMap<Int, String> = HashMap()
    override fun onBeforeContextMenu(
        browser: CefBrowser, frame: CefFrame, params: CefContextMenuParams, model: CefMenuModel
    ) {
        model.clear()

        // Navigation menu
        model.addItem(CefMenuModel.MenuId.MENU_ID_BACK, "Back")
        model.setEnabled(CefMenuModel.MenuId.MENU_ID_BACK, browser.canGoBack())
        model.addItem(CefMenuModel.MenuId.MENU_ID_FORWARD, "Forward")
        model.setEnabled(CefMenuModel.MenuId.MENU_ID_FORWARD, browser.canGoForward())
        model.addSeparator()
        model.addItem(CefMenuModel.MenuId.MENU_ID_FIND, "Find...")
        if (params.hasImageContents() && params.sourceUrl != null) model.addItem(
            CefMenuModel.MenuId.MENU_ID_USER_FIRST,
            "Download Image..."
        )
        model.addItem(CefMenuModel.MenuId.MENU_ID_VIEW_SOURCE, "View Source...")
        val suggestions: Vector<String> = Vector<String>()
        params.getDictionarySuggestions(suggestions)

        // Spell checking menu
        model.addSeparator()
        if (suggestions.size == 0) {
            model.addItem(CefMenuModel.MenuId.MENU_ID_NO_SPELLING_SUGGESTIONS, "No suggestions")
            model.setEnabled(CefMenuModel.MenuId.MENU_ID_NO_SPELLING_SUGGESTIONS, false)
            return
        }
        var id: Int = CefMenuModel.MenuId.MENU_ID_SPELLCHECK_SUGGESTION_0
        for (suggestedWord in suggestions) {
            model.addItem(id, suggestedWord)
            this.suggestions[id] = suggestedWord
            if (++id > CefMenuModel.MenuId.MENU_ID_SPELLCHECK_SUGGESTION_LAST) break
        }
    }

    override fun onContextMenuCommand(
        browser: CefBrowser, frame: CefFrame,
        params: CefContextMenuParams, commandId: Int, eventFlags: Int
    ): Boolean {
        return when (commandId) {
            CefMenuModel.MenuId.MENU_ID_VIEW_SOURCE -> {
                val visitor = ShowTextDialog(owner_, "Source of \"" + browser.url + "\"")
                browser.getSource(visitor)
                true
            }
            CefMenuModel.MenuId.MENU_ID_FIND -> {
                val search = SearchDialog(owner_, browser)
                search.isVisible = true
                true
            }
            CefMenuModel.MenuId.MENU_ID_USER_FIRST -> {
                browser.startDownload(params.sourceUrl)
                true
            }
            else -> {
                if (commandId >= CefMenuModel.MenuId.MENU_ID_SPELLCHECK_SUGGESTION_0) {
                    val newWord = suggestions[commandId]
                    if (newWord != null) {
                        System.err.println(
                            "replacing " + params.misspelledWord + " with " + newWord
                        )
                        browser.replaceMisspelling(newWord)
                        return true
                    }
                }
                false
            }
        }
    }

    override fun onContextMenuDismissed(browser: CefBrowser, frame: CefFrame) {
        suggestions.clear()
    }
}