// Copyright (c) 2014 The Chromium Embedded Framework Authors. All rights
// reserved. Use of this source code is governed by a BSD-style license that
// can be found in the LICENSE file.
package com.besttech.test.handler

import org.cef.callback.CefCallback
import org.cef.handler.CefResourceHandlerAdapter
import org.cef.misc.IntRef
import org.cef.misc.StringRef
import org.cef.network.CefRequest
import org.cef.network.CefResponse
import java.io.ByteArrayOutputStream
import java.io.IOException

/**
 * The example for the second scheme with domain handling is a more
 * complex example and is taken from the parent project CEF. Please
 * see CEF: "cefclient/scheme_test.cpp" for futher details
 */
class ClientSchemeHandler : CefResourceHandlerAdapter() {
    private lateinit var data_: ByteArray
    private var mime_type_: String? = null
    private var offset_ = 0
    @Synchronized
    override fun processRequest(request: CefRequest, callback: CefCallback): Boolean {
        var handled = false
        val url = request.url
        if (url.indexOf("handler.html") != -1) {
            // Build the response html
            var html: String
            html = ("<html><head><title>Client Scheme Handler</title></head>"
                    + "<body bgcolor=\"white\">"
                    + "This contents of this page page are served by the "
                    + "ClientSchemeHandler class handling the client:// protocol."
                    + "<br/>You should see an image:"
                    + "<br/><img src=\"client://tests/logo.png\"><pre>")

            // Output a string representation of the request
            html += request.toString()
            html += ("</pre><br/>Try the test form:"
                    + "<form method=\"POST\" action=\"handler.html\">"
                    + "<input type=\"text\" name=\"field1\">"
                    + "<input type=\"text\" name=\"field2\">"
                    + "<input type=\"submit\">"
                    + "</form></body></html>")
            data_ = html.toByteArray()
            handled = true
            // Set the resulting mime type
            mime_type_ = "text/html"
        } else if (url.endsWith(".png")) {
            handled = loadContent(url.substring(url.lastIndexOf('/') + 1))
            mime_type_ = "image/png"
        } else if (url.endsWith(".html")) {
            handled = loadContent(url.substring(url.lastIndexOf('/') + 1))
            mime_type_ = "text/html"
            if (!handled) {
                var html = "<html><head><title>Error 404</title></head>"
                html += "<body><h1>Error 404</h1>"
                html += "File  " + url.substring(url.lastIndexOf('/') + 1) + " "
                html += "does not exist</body></html>"
                data_ = html.toByteArray()
                handled = true
            }
        }
        if (handled) {
            // Indicate the headers are available.
            callback.Continue()
            return true
        }
        return false
    }

    override fun getResponseHeaders(
        response: CefResponse, response_length: IntRef, redirectUrl: StringRef
    ) {
        response.mimeType = mime_type_
        response.status = 200

        // Set the resulting response length
        response_length.set(data_.size)
    }

    @Synchronized
    override fun readResponse(
        data_out: ByteArray, bytes_to_read: Int, bytes_read: IntRef, callback: CefCallback
    ): Boolean {
        var has_data = false
        if (offset_ < data_.size) {
            // Copy the next block of data into the buffer.
            val transfer_size = Math.min(bytes_to_read, data_.size - offset_)
            System.arraycopy(data_, offset_, data_out, 0, transfer_size)
            offset_ += transfer_size
            bytes_read.set(transfer_size)
            has_data = true
        } else {
            offset_ = 0
            bytes_read.set(0)
        }
        return has_data
    }

    private fun loadContent(resName: String): Boolean {
        val inStream = javaClass.getResourceAsStream(resName)
        if (inStream != null) {
            try {
                val outFile = ByteArrayOutputStream()
                var readByte = -1
                while (inStream.read().also { readByte = it } >= 0) outFile.write(readByte)
                data_ = outFile.toByteArray()
                return true
            } catch (e: IOException) {
            }
        }
        return false
    }

    companion object {
        const val scheme = "client"
        const val domain = "tests"
    }
}