// Copyright (c) 2014 The Chromium Embedded Framework Authors. All rights
// reserved. Use of this source code is governed by a BSD-style license that
// can be found in the LICENSE file.
package com.besttech.test.handler

import org.cef.browser.CefBrowser
import org.cef.browser.CefFrame
import org.cef.callback.CefQueryCallback
import org.cef.handler.CefMessageRouterHandlerAdapter

class MessageRouterHandler : CefMessageRouterHandlerAdapter() {
    override fun onQuery(
        browser: CefBrowser, frame: CefFrame, query_id: Long, request: String,
        persistent: Boolean, callback: CefQueryCallback
    ): Boolean {
        if (request.indexOf("BindingTest:") == 0) {
            // Reverse the message and return it to the JavaScript caller.
            val msg = request.substring(12)
            callback.success(StringBuilder(msg).reverse().toString())
            return true
        }
        // Not handled.
        return false
    }
}