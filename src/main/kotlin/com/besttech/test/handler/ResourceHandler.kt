package com.besttech.test.handler

import org.cef.callback.CefCallback
import org.cef.handler.CefResourceHandlerAdapter
import org.cef.misc.IntRef
import org.cef.misc.StringRef
import org.cef.network.CefRequest
import org.cef.network.CefResponse
import java.nio.ByteBuffer

class ResourceHandler : CefResourceHandlerAdapter() {
    private var startPos = 0
    override fun processRequest(request: CefRequest, callback: CefCallback): Boolean {
        println("processRequest: $request")
        startPos = 0
        callback.Continue()
        return true
    }

    override fun getResponseHeaders(
        response: CefResponse, response_length: IntRef, redirectUrl: StringRef
    ) {
        println("getResponseHeaders: $response")
        response_length.set(html.length)
        response.mimeType = "text/html"
        response.status = 200
    }

    override fun readResponse(
        data_out: ByteArray, bytes_to_read: Int, bytes_read: IntRef, callback: CefCallback
    ): Boolean {
        val length = html.length
        if (startPos >= length) return false

        // Extract up to bytes_to_read bytes from the html data
        val endPos = startPos + bytes_to_read
        val dataToSend = if (endPos > length) html.substring(startPos) else html.substring(startPos, endPos)

        // Copy extracted bytes into data_out and set the read length
        // to bytes_read.
        val result = ByteBuffer.wrap(data_out)
        result.put(dataToSend.toByteArray())
        bytes_read.set(dataToSend.length)
        startPos = endPos
        return true
    }

    override fun cancel() {
        println("cancel")
        startPos = 0
    }

    companion object {
        private const val html: String =
            """<html>
              <head>
                <title>ResourceHandler Test</title>
              </head>
              <body>
                <h1>ResourceHandler Test</h1>
                <p>You have entered the URL: http://www.foo.bar. This page is generated by the application itself and<br/>
                   no HTTP request was sent to the internet.
                <p>See class <u>tests.handler.ResourceHandler</u> and the <u>RequestHandler</u> implementation for details.</p>
              </body>
            </html>"""
    }
}