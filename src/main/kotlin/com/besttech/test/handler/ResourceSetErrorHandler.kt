package com.besttech.test.handler

import org.cef.callback.CefCallback
import org.cef.handler.CefLoadHandler
import org.cef.handler.CefResourceHandlerAdapter
import org.cef.misc.IntRef
import org.cef.misc.StringRef
import org.cef.network.CefRequest
import org.cef.network.CefResponse

class ResourceSetErrorHandler : CefResourceHandlerAdapter() {
    override fun processRequest(request: CefRequest, callback: CefCallback): Boolean {
        println("processRequest: $request")
        callback.Continue()
        return true
    }

    override fun getResponseHeaders(
        response: CefResponse, response_length: IntRef, redirectUrl: StringRef
    ) {
        response.error = CefLoadHandler.ErrorCode.ERR_NOT_IMPLEMENTED
        println("getResponseHeaders: $response")
    }
}