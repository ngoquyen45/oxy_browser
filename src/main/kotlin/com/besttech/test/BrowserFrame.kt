// Copyright (c) 2018 The Chromium Embedded Framework Authors. All rights
// reserved. Use of this source code is governed by a BSD-style license that
// can be found in the LICENSE file.
package com.besttech.test

import org.cef.CefApp
import org.cef.browser.CefBrowser
import org.cef.handler.CefLifeSpanHandlerAdapter
import java.awt.event.WindowAdapter
import java.awt.event.WindowEvent
import javax.swing.JFrame
import javax.swing.SwingUtilities

open class BrowserFrame @JvmOverloads constructor(title: String? = null) : JFrame(title) {
    private var isClosed = false
    private var cefBrowser: CefBrowser? = null
    private var afterParentChangedAction: Runnable? = null
    fun removeBrowser(r: Runnable?) {
        println("BrowserFrame.removeBrowser")
        afterParentChangedAction = r
        remove(cefBrowser!!.uiComponent)
        // The removeNotify() notification should be sent as a result of calling remove().
        // However, it isn't in all cases so we do it manually here.
        cefBrowser!!.uiComponent.removeNotify()
        cefBrowser = null
    }

    var browser: CefBrowser?
        get() = cefBrowser
        set(browser) {
            if (cefBrowser == null) cefBrowser = browser
            cefBrowser!!.client.removeLifeSpanHandler()
            cefBrowser!!.client.addLifeSpanHandler(object : CefLifeSpanHandlerAdapter() {
                override fun onAfterCreated(browser: CefBrowser) {
                    println("BrowserFrame.onAfterCreated id=" + browser.identifier)
                    browserCount_++
                }

                override fun onAfterParentChanged(browser: CefBrowser) {
                    println(
                        "BrowserFrame.onAfterParentChanged id=" + browser.identifier
                    )
                    if (afterParentChangedAction != null) {
                        SwingUtilities.invokeLater(afterParentChangedAction)
                        afterParentChangedAction = null
                    }
                }

                override fun doClose(browser: CefBrowser): Boolean {
                    val result = browser.doClose()
                    println(
                        "BrowserFrame.doClose id=" + browser.identifier
                                + " CefBrowser.doClose=" + result
                    )
                    return result
                }

                override fun onBeforeClose(browser: CefBrowser) {
                    println("BrowserFrame.onBeforeClose id=" + browser.identifier)
                    if (--browserCount_ == 0) {
                        println("BrowserFrame.onBeforeClose CefApp.dispose")
                        CefApp.getInstance().dispose()
                    }
                }
            })
        }

    companion object {
        private var browserCount_ = 0
    }

    init {

        // Browser window closing works as follows:
        //   1. Clicking the window X button calls WindowAdapter.windowClosing.
        //   2. WindowAdapter.windowClosing calls CefBrowser.close(false).
        //   3. CEF calls CefLifeSpanHandler.doClose() which calls CefBrowser.doClose()
        //      which returns true (canceling the close).
        //   4. CefBrowser.doClose() triggers another call to WindowAdapter.windowClosing.
        //   5. WindowAdapter.windowClosing calls CefBrowser.close(true).
        //   6. For windowed browsers CEF destroys the native window handle. For OSR
        //      browsers CEF calls CefLifeSpanHandler.doClose() which calls
        //      CefBrowser.doClose() again which returns false (allowing the close).
        //   7. CEF calls CefLifeSpanHandler.onBeforeClose and the browser is destroyed.
        //
        // On macOS pressing Cmd+Q results in a call to CefApp.handleBeforeTerminate
        // which calls CefBrowser.close(true) for each existing browser. CEF then calls
        // CefLifeSpanHandler.onBeforeClose and the browser is destroyed.
        //
        // Application shutdown works as follows:
        //   1. CefLifeSpanHandler.onBeforeClose calls CefApp.getInstance().dispose()
        //      when the last browser window is destroyed.
        //   2. CefAppHandler.stateHasChanged terminates the application by calling
        //      System.exit(0) when the state changes to CefAppState.TERMINATED.
        addWindowListener(object : WindowAdapter() {
            override fun windowClosing(e: WindowEvent) {
                if (cefBrowser == null) {
                    // If there's no browser we can dispose immediately.
                    isClosed = true
                    println("BrowserFrame.windowClosing Frame.dispose")
                    dispose()
                    return
                }
                val isClosed = isClosed
                if (isClosed) {
                    // Cause browser.doClose() to return false so that OSR browsers
                    // can close.
                    cefBrowser!!.setCloseAllowed()
                }

                // Results in another call to this method.
                println("BrowserFrame.windowClosing CefBrowser.close($isClosed)")
                cefBrowser!!.close(isClosed)
                if (!this@BrowserFrame.isClosed) {
                    this@BrowserFrame.isClosed = true
                }
                if (isClosed) {
                    // Dispose after the 2nd call to this method.
                    println("BrowserFrame.windowClosing Frame.dispose")
                    dispose()
                }
            }
        })
    }
}